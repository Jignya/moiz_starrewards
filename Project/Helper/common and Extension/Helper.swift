//
//  Helper.swift
//  Maki
//
//  Created by Pooja Patel on 28/06/16.
//  Copyright © 2016 olivermaki. All rights reserved.
//

import UIKit
import SystemConfiguration

class Helper: NSObject
{
    
    fileprivate var reach:Reachability!
    var networkStatus = Reachability()!
    var isInternetConnected: Bool!
    
    static let sharedInstance = Helper()
    
    // MARK: ProgressHud
    func ShowProgressBarWithText(_ loadingText:String) -> Void
    {
        UIApplication.shared.beginIgnoringInteractionEvents()
        
//        GIFHUD.shared.setGif(named: "loading.gif")
//        GIFHUD.shared.show(withOverlay: true, duration: nil)

        
        // update some UI
        let win:UIWindow = ((UIApplication.shared.delegate?.window)!)!
        
        let vwIndi = UIView(frame: win.bounds) as UIView
        vwIndi.tag = 11111;
        
        let hud = MBProgressHUD.showAdded(to: vwIndi, animated: true)
        hud.contentColor = UIColor.white
//        hud.contentColor = GlobalConstants.lightTheme
        hud.backgroundColor = UIColor.clear
        hud.tintColor =  UIColor.clear
        hud.customView?.center = vwIndi.center
        //        hud.mode = .annularDeterminate
        hud.backgroundView.color = UIColor.clear
        hud.backgroundView.style = .solidColor
        hud.bezelView.color = GlobalConstants.DarkTheme
        
        vwIndi.frame = win.frame
        vwIndi.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        win.addSubview(vwIndi)
        
        vwIndi.isUserInteractionEnabled = true;
        win.isUserInteractionEnabled = false;
        
    }
    
    func DismissProgressBar()
    {
//        DispatchQueue.main.async
//        {
//
//            GIFHUD.shared.dismiss()
//        }
        
        if UIApplication.shared.isIgnoringInteractionEvents
        {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        DispatchQueue.main.async
            {
                let win:UIWindow = UIApplication.shared.delegate!.window!!
                
                //        if win.viewWithTag(1111) != nil {
                let vwIndi = (win.viewWithTag(11111))! as UIView
                MBProgressHUD.hide(for: vwIndi, animated: true)
                win.isUserInteractionEnabled = true;
                
                vwIndi.removeFromSuperview()
                if UIApplication.shared.isIgnoringInteractionEvents
                {
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
        }
    }
    
    // MARK: Reachability
    
    func hasConnectivity() -> Bool
    {
        
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"checkForReachability:", name: ReachabilityChangedNotification, object: nil);
        //
        //        do
        //        {
        //            self.reach = try Reachability.reachabilityForInternetConnection()
        //        }
        //        catch{}
        //        do
        //        {
        //            try self.reach.startNotifier()
        //        }
        //        catch{}
        
        do {
            reach = Reachability()
            let networkStatus: Int = reach.connection.hashValue
            
            return (networkStatus != 0)
        }
        catch {
            // Handle error however you please
            return false
        }
    }
    
    
    func checkForReachability(_ notification:Notification)
    {
        networkStatus = Reachability()!
        networkStatus.whenReachable = { networkStatus in
            if networkStatus.connection == .wifi {
                print("Reachable in Helper via WiFi")
            } else {
                print("Reachable in Helper via Cellular")
            }
            self.isInternetConnected = true
        }
        networkStatus.whenUnreachable = { _ in
            print("Not reachable")
            self.isInternetConnected = false
        }
        
        //        if (networkStatus.NetworkStatus == Reachability.NetworkUnreachable)
        //        {
        //            print("Not Reachable in Helper")
        //            isInternetConnected = false
        //
        //        }
        //        else
        //        {
        //            print("Reachable in Helper")
        //            isInternetConnected = true
        //        }
    }
}


