//
//  ConstantLabel.swift
//  IHallab
//
//  Created by Monali on 12/6/16.
//  Copyright © 2016 Monali. All rights reserved.
//

import Foundation

struct CommanConstants {
    // Constant define here.
    
//    static let kWarningEn = "Warning"
    static let kWarningAr = "تحذير"
    static let kYesEn = "Yes"
    static let kYesAr = "نعم"
    static let kNoEn = "No"
    static let kNoAr = "لا"
    static let kSuccessEn = "Successfully"
    static let kSuccessAr = "بنجاح"
  
    
    static let kUId = "UserId"
    static let kUserEmail = "UserEmail"
    static let kUserFirstName = "UserFName"
    
    static var kSelectedLanguage = String(format: "%@",UserDefaults.standard.object(forKey: "SelectedLanguage") as! String)
    
}
var kLoading:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "جاري التحميل"
    }
    else
    {
        return "Loading"
    }
}
var kYes:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نعم"
    }
    else
    {
        return "Yes"
        
    }
}
var kNo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لا"
    }
    else
    {
        return "NO"
        
    }
}
var kWarningEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تحذير"
    }
    else
    {
        return "Warning"
        
    }
}

var kDeleteEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حذف"
    }
    else
    {
        return "Delete"
        
    }
}

var kAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تنبيه"
    }
    else
    {
        return "Alert"
        
    }
}

var kMenu:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "القائمة"
    }
    else
    {
        return "Menu"
        
    }
}

var kCatelogue:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "فهرس"
    }
    else
    {
        return "Catalogue"
        
    }
}

var kEnglish:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "English"
    }
    else
    {
        return "English"
        
    }
}
var kArab:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عربي"
    }
    else
    {
        return "عربي"
        
    }
}

var kOkEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "موافق"
    }
    else
    {
        return "OK"
        
    }
}
var kShareFeedback:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مشاركه ملاحظاتك"
    }
    else
    {
        return "Share your feedback"
        
    }
}

var kCategory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الفئة"
    }
    else
    {
        return "Category"
        
    }
}

var kFavorite:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المفضل"
    }
    else
    {
        return "Favorites"
        
    }
}
var kMobile:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هاتف المحمول"
    }
    else
    {
        return "Mobile"
        
    }
}
var kEmail:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "البريد الالكتروني"
    }
    else
    {
        return "Email"
        
    }
}
var kFullname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم الكامل"
    }
    else
    {
        return "Full Name"
        
    }
}
var kNoItem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السلة الفارغة"
    }
    else
    {
        return "No Items available"
    }
}

var kPleaseenterFullnameEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال الاسم الكامل"
    }
    else
    {
        return "Please enter fullname"
        
    }
}
var kPleaseenteryouremailaddressEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال البريد الالكتروني"
    }
    else
    {
        return "Please enter your email"
        
    }
}


var kNameValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يمكنك إدخال الحد الأدنى 3 حرف واقصي 30 حرف للاسم"
    }
    else
    {
        return "You can enter min 3 character and max 30 character for name"
        
    }
}
var kPleaseentervalidMobnum:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال رقم الجوال الصحيح"
    }
    else
    {
        return "Please enter valid mobile number"
        
    }
}
var kEmailValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال البريد الالكتروني الصحيح"
    }
    else
    {
        return "Please enter a valid email"
        
    }
}
var kfeedbackValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى إدخال ملاحظاتك"
    }
    else
    {
        return "Please enter your feedback"
        
    }
}
var kfeedback:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ردود الفعل"
    }
    else
    {
        return "Feedback"
        
    }
}
var kfeedbackSuccess:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ردود الفعل المقدمة بنجاح"
    }
    else
    {
        return "Feedback submitted succesfully"
        
    }
}
var kremoveAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ستتم إزالة جميع العناصر المفضلة إذا قمت بالضغط على حسنا"
    }
    else
    {
        return "All favorite items will be removed if you press ok"
        
    }
}
var kCancel:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الغاء"
    }
    else
    {
        return "Cancel"
        
    }
}
var kConfirmOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أكد الطلب"
    }
    else
    {
        return "Confirm Order"
        
    }
}
var kaddtoOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضافة للأمر"
    }
    else
    {
        return "Add to Order"
        
    }
}
var kPreview:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معاينة"
    }
    else
    {
        return "Preview"
        
    }
}
var kTotal:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاجمالي"
    }
    else
    {
        return "Total"
        
    }
}
var kTotalAmount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المبلغ الاجمالي"
    }
    else
    {
        return "Total Amount"
        
    }
}

var kCurrency:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "د.ك"
    }
    else
    {
        return "KWD"
        
    }
}
var kQty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الكميه"
    }
    else
    {
        return "Qty"
        
    }
}
var kPrice:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السعر"
    }
    else
    {
        return "Price"
    }
        
}

var aplaceOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "انت متاكد من اتمام الطلب"
    }
    else
    {
        return "Are you sure you want to place order?"
    }
    
}
var kThankyou:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شكرًا"
    }
    else
    {
        return "Thank you!"
    }
    
}
var aConfirmOrderSuccess:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طلبك مؤكد!"
    }
    else
    {
        return "Your Order Confirmed!"
    }
    
}
var aEnterPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال كلمة السر"
    }
    else
    {
        return "Please enter your password"
    }
    
}

var kNorecord:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لايوجد طلب"
    }
    else
    {
        return "No order found"
    }
    
}
var kOrderMsg:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أجل معرف الخاص بك هو"
    }
    else
    {
        return "Your Order id is"
    }
    
}
var kreset:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اعادة تشغيل"
    }
    else
    {
        return "RESET"
    }
    
}
var kNext:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التالي"
    }
    else
    {
        return "NEXT"
    }
    
}
var kentertocontinue:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أدخل كلمة المرور للمتابعة"
    }
    else
    {
        return "ENTER PASSWORD TO CONTINUE"
    }
    
}


var kselectQty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر الكميه"
    }
    else
    {
        return "Select your quantity"
    }
    
}
var kAddOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أضف طلب"
    }
    else
    {
        return "Add Order"
    }
    
}

var kPleaseenteryourmobileEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال رقم الهاتف المحمول"
    }
    else
    {
        return "Please enter your mobile number"
        
    }
}
var kPleaseenteryourpasswordEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال كلمة السر"
    }
    else
    {
        return "Please enter your password"
        
    }
}

var kPasswordValidEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يجب ان تحتوي كلمه المرور علي 8 أحرف علي الأقل"
    }
    else
    {
        return "Password should contain atleast 8 characters"
        
    }
}
var kPleaseenteryourConfirmpasswordEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء اعادة ادخال كلمة السر"
    }
    else
    {
        return "Please re-enter  password"
        
    }
}
var kpasswordmismatchEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة السر لم تتطابق"
    }
    else
    {
        return "Password does not match"
        
    }
}
var kYouhavesuccessfullyregistered:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم التسجيل بنجاح"
    }
    else
    {
        return "You have successfully registered"
        
    }
}
var kPleaseenteryourNameEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال الاسم"
    }
    else
    {
        return "Please enter name"
        
    }
}
var kPleaseenterAddresssname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال اسم للعنوان"
    }
    else
    {
        return "Please enter address name"
        
    }
}
var kPleaseSelectArea:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء اختر المنطقة"
    }
    else
    {
        return "Please select area"
        
    }
}
var kPleaseSelectblock:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال القطعة"
    }
    else
    {
        return "Please enter block"
    }
}
var kPleaseenterStreet: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال الشارع"
    }
    else
    {
        return "Please enter street name"
    }
}
var kPleaseenterhouse: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال المنزل"
    }
    else
    {
        return "Please enter house name"
    }
}
var kPleaseenteraprtmnt: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال رقم الشقة"
    }
    else
    {
        return "Please enter appartment"
    }
}
var kviewDetails:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عرض التفاصيل"
    }
    else
    {
        return "View Detail"
        
    }
}
var kDeleteadd:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل متاكد من حذف العنوان؟"
    }
    else
    {
        return "Are you sure you want to delete address?"
    }
        
}
var aOrderPlaced:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لقد تم الطلب بنجاح.رقم الطلب"
    }
    else
    {
        return "Your order has been placed successfully.Your order Id is"
    }
    
}

var kTerms:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أحكام وشروط"
    }
    else
    {
        return "Terms and Condition"
    }
    
}
var kAbout:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نبذة عنا"
    }
    else
    {
        return "About us"
    }
    
}
var kDelPolicy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شروط التوصيل"
    }
    else
    {
        return "Delivery Policy"
    }
    
}
var kreturnPolicy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شروط التبديل"
    }
    else
    {
        return "Return Policy"
    }
    
}
var kPaynow:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ادفع الان"
    }
    else
    {
        return "Pay Now"
    }
    
}
var kOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطلب"
    }
    else
    {
        return "Order"
    }
    
}
var kPassword:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة السر"
    }
    else
    {
        return "Password"
    }
    
}
var kLogin:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول"
    }
    else
    {
        return "Login"
    }
}
var kForgotPW:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نسيت كلمه المرور ؟"
    }
    else
    {
        return "Forgot Password?"
    }
}
var kLoginwith:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول مع"
    }
    else
    {
        return "LOGIN WITH"
    }
}
var kDontAccount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ليس لديك حساب ؟"
    }
    else
    {
        return "Don't have an account?"
    }
}
var kregister:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل"
    }
    else
    {
        return "Register"
    }
}
var kIhaveAcnt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لدي حساب"
    }
    else
    {
        return "I have an account"
    }
}

var ksignupwith:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اشترك مع"
    }
    else
    {
        return "SIGN UP WITH"
    }
}
var kName:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم"
    }
    else
    {
        return "Name"
    }
}
var kSubmit:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ارسال"
    }
    else
    {
        return "Submit"
    }
}
var kForgotTitle:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نسيت كلمه المرور "
    }
    else
    {
        return "FORGOT PASSWORD"
    }
}
var kForgotDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ادخل عنوان بريدك الكتروني المسجل. سوف نرسل لك أعاده تعيين كلمه المرور أو كلمه المرور الجديدة."
    }
    else
    {
        return "Enter your registered email address.we will send you reset password link or new password."
    }
}
var kSelcat:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر القائمة "
    }
    else
    {
        return "Select Categories"
    }
}
var kSearch:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بحث"
    }
    else
    {
        return "Search"
    }
}
var kBuyNow:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اشتر الان"
    }
    else
    {
        return "Buy Now"
    }
}
var kQuantity:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الكمية"
    }
    else
    {
        return "Quantity"
    }
}
var kItemSize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "القياس"
    }
    else
    {
        return "Item Size"
    }
}
var kItemname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم العنصر"
    }
    else
    {
        return "Item Name"
    }
}
var kItemDetail:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تفاصيل البند"
    }
    else
    {
        return "Item Detail"
    }
}
var kItemDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "وصف السلعة"
    }
    else
    {
        return "Item Description"
    }
}
var kAddtocart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضافة السلة"
    }
    else
    {
        return "Add to cart"
    }
}
var kGotoCart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الذهاب الى السلة"
    }
    else
    {
        return "Go to cart"
    }
}
var kHome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الصفحة الرئيسية"
    }
    else
    {
        return "Home"
    }
}
var kMyOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طلبي"
    }
    else
    {
        return "My Order"
    }
}

var kMycart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سلتي"
    }
    else
    {
        return "My Cart"
    }
}
var kwish:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الأماني"
    }
    else
    {
        return "Wishlist"
    }
}
var kNotification:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إعلام"
    }
    else
    {
        return "Notification"
    }
}
var ksetting:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاعدادات"
    }
    else
    {
        return "Setting"
    }
}

var kAppInside:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التطبيق في الداخل"
    }
    else
    {
        return "App Inside"
    }
}
var kChangePW:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تغير كلمة السر"
    }
    else
    {
        return "Change Password"
    }
}
var kSavedAdd:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العنوان المسجل"
    }
    else
    {
        return "Saved Address"
    }
}
var kprofile:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الملف الشخصي"
    }
    else
    {
        return "Profile"
    }
}
var klang:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اللغة"
    }
    else
    {
        return "Language"
    }
}
var kExchange:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسترجاع والتبديل"
    }
    else
    {
        return "Exchange & Return"
    }
}
var kprivacy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سياسة خاصة"
    }
    else
    {
        return "Privacy Policy"
    }
}
var kShipping:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معلومات التوصيل"
    }
    else
    {
        return "Shipping Information"
    }
}
var ksell:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بيع معنا"
    }
    else
    {
        return "Sell with us"
    }
}
var kcontact:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اتصل بنا"
    }
    else
    {
        return "Contact us"
    }
}
var kOrders:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطلبات"
    }
    else
    {
        return "Orders"
    }
}
var kNocategory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لم يتم العثور على القائمة"
    }
    else
    {
        return "No Categories found"
    }
}
var kmostRecent:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطلبات الحديثة"
    }
    else
    {
        return "Most recent order"
    }
}
var kTotalPaid:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اجمال المبلغ المدفوع"
    }
    else
    {
        return "Total Paid"
    }
}
var kOrderSummary:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ملخص الطلب"
    }
    else
    {
        return "Order Summary"
    }
}
var kDate:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التاريخ"
    }
    else
    {
        return "Date"
    }
}
var kTime:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الوقت"
    }
    else
    {
        return "Time"
    }
}

var kOrderNumber:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم الطلب"
    }
    else
    {
        return "Order Number"
    }
}
var kPaidBy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "دفعت بواسطة"
    }
    else
    {
        return "Paid By"
    }
}

var kDelCharges:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رسوم التوصيل"
    }
    else
    {
        return "Delivery Charges"
    }
}
var kShoppingBag:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سلة التسوق"
    }
    else
    {
        return "Shopping Bag"
    }
}
var kBagEmpty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السلة فارغة"
    }
    else
    {
        return "Your bag is empty"
    }
}

var kPlaceOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضف طلب"
    }
    else
    {
        return "Place Order"
    }
}
var ksize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المقاس"
    }
    else
    {
        return "Size"
    }
}
var kaddress:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العنوان"
    }
    else
    {
        return "Address"
    }
}
var kaddressname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم العنوان"
    }
    else
    {
        return "Address Name"
    }
}
var kArea:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المنطقة"
    }
    else
    {
        return "Area"
    }
}
var kblock:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "القطعة"
    }
    else
    {
        return "Block"
    }
}
var kStreet:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الشارع"
    }
    else
    {
        return "Street"
    }
}
var kStreetname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم /رقم الشارع"
    }
    else
    {
        return "Street Name/Number"
    }
}
var kHousNo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم المنزل"
    }
    else
    {
        return "House No"
    }
}
var kHousNoOptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم المنزل (اختياري)"
    }
    else
    {
        return "House No(Optional)"
    }
}
var kAvenue:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الجادة"
    }
    else
    {
        return "Avenue"
    }
}
var kAvenueoptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الجادة (اختياري)"
    }
    else
    {
        return "Avenue(Optional)"
    }
}
var kBuilding:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المبنى"
    }
    else
    {
        return "Building"
    }
}
var kBuildingOptioanl:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المبنى (اختياري)"
    }
    else
    {
        return "Building(Optional)"
    }
}
var kFloor:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدور"
    }
    else
    {
        return "Floor"
    }
}
var kFloorOptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدور (اختياري)"
    }
    else
    {
        return "Floor(Optional)"
    }
}
var kAppartment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شقة"
    }
    else
    {
        return "Appartment"
    }
}
var kExtraDir:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ارشادات اضافية (اختياري)"
    }
    else
    {
        return "Extra direction(Optional)"
    }
}
var kSaveChanges:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حفظ التعديلات"
    }
    else
    {
        return "Save Changes"
    }
}
var kNewPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة سر جديدة"
    }
    else
    {
        return "New password"
    }
}
var kConfirmPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تاكيد كلمة السر"
    }
    else
    {
        return "Confirm password"
    }
}
var kcod:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نقدًا"
    }
    else
    {
        return "Cash On Delivery"
    }
}
var kKnetPayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كي-نت"
    }
    else
    {
        return "Knet Payment"
    }
}
var kCredit:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بطاقة الائتمان"
    }
    else
    {
        return "Credit Card"
    }
}
var kItems:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "منتج"
    }
    else
    {
        return "Items"
    }
}
var kTotalPayable:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المجموع"
    }
    else
    {
        return "Total Payable"
    }
}
var kChange:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تعديل"
    }
    else
    {
        return "Change"
    }
}
var kPaymentoption:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طريقة الدفع"
    }
    else
    {
        return "Payment Options"
    }
}
var kDeliverTo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "توصيل الى"
    }
    else
    {
        return "Deliver To"
    }
}
var kPayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدفع"
    }
    else
    {
        return "Payment"
    }
}
var kTransaction:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم العملية"
    }
    else
    {
        return "Transaction Number"
    }
}
var kOrderStatus:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حالة الطلب"
    }
    else
    {
        return "Order Status"
    }
}
var kSave:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حفظ"
    }
    else
    {
        return "Save"
    }
}
var kPaymentreceipt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الفاتورة"
    }
    else
    {
        return "Payment Receipt"
    }
}
var kDone:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم"
    }
    else
    {
        return "Done"
    }
}
var knotLoggedin:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "غير مسجل حاليًا"
    }
    else
    {
        return "You are not currently logged in"
    }
}
var kGuest:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "زائر"
    }
    else
    {
        return "Guest"
    }
}
var kSignIn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول"
    }
    else
    {
        return "Sign In"
    }
}
var kItemAdded:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم اضافة المنتج بنجاح"
    }
    else
    {
        return "Item added to cart successfully"
    }
}
var kRemoveItem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "متاكد من حذف المنتج ؟"
    }
    else
    {
        return "Are you sure want to remove this item from cart?"
    }
}
var kUnread:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "غير مقروء"
    }
    else
    {
        return "Unread"
    }
}
var kcancelorder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إذا تركت هذه الصفحة ، سيتم إلغاء طلبك. اختر نعم للمتابعة."
    }
    else
    {
        return "If you leave this page,Your order will be cancelled.Click yes to continue."
    }
}
var kReceiveOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سوف تتلقي الطلب وفقا لعمليه التسليم لدينا"
    }
    else
    {
        return "you will receive order as per our standard delivery process"
    }
}
var kForpayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "للدفع"
    }
    else
    {
        return "For the payment"
    }
}

var kThanku:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شكرًا"
    }
    else
    {
        return "THANK YOU"
    }
}

var kViewHistory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سجل الدفع"
    }
    else
    {
        return "View your payment history"
    }
}

var kError:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هناك خطا  بالدفع ، يرجى المحاولة مره أخرى مع طريقه دفع مختلفة."
    }
    else
    {
        return "There was an error with payment, please try again with different payment method."
    }
}

var kRedirecttoHome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سيتم أعاده توجيهك إلى شاشه الدفع مره أخرى بعد 5 ثوان..."
    }
    else
    {
        return "You will be redirect to payment screen again after 5 seconds..."
    }
}

var kConnecttoInternet:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اتصل بالانترنت"
    }
    else
    {
        return "Connect to the Internet"
    }
}
var kCheckConnection:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى التاكد من اتصالك بالانترنت"
    }
    else
    {
        return "You're offline. Check your connection"
    }
}
var kretry:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حاول مرة اخرى"
    }
    else
    {
        return "Retry"
    }
}
var kExtraDirection:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ارشادات اخرى"
    }
    else
    {
        return "Extra Direction"
    }
}
var kwelcome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اهلاً وسهلاً"
    }
    else
    {
        return "Welcome"
    }
}
var kStandard:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التوصيل المحلي"
    }
    else
    {
        return "Standard Delivery"
    }
}
var kproductAmt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كمية المنتج"
    }
    else
    {
        return "Product Amount"
    }
}
var kLogoutalert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل متاكد من الخروج ؟"
    }
    else
    {
        return "Are you sure want to logout?"
    }
}
var kfororder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لطلبك"
    }
    else
    {
        return "For your order"
    }
}
var kFav:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لطلبك"
    }
    else
    {
        return "Favourites"
    }
}
var kVersion:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المفضله"
    }
    else
    {
        return "Version"
    }
}
var kQtyAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return ""
    }
    else
    {
        return "Available qty is %@ , you cant select more than that"
    }
}

var kLogout:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الخروج"
    }
    else
    {
        return "Logout"
    }
}
var kTier:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "فوائد الطبقة"
    }
    else
    {
        return "Tier benefits"
    }
}
var kEnter:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ادخل"
    }
    else
    {
        return "Enter Your"
    }
}
var kMobNum:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم الهاتف المحمول"
    }
    else
    {
        return "Mobile Number"
    }
}
var kVerificationDesc1:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم إرسال الرسائل القصيرة إلى هاتفك المحمول"
    }
    else
    {
        return "A sms has been sent to your mobile"
    }
}
var kVerificationDesc2:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى إدخال رقم التعريف الشخصي الذي تم استلامه في اليوم التالي"
    }
    else
    {
        return "Please enter the PIN received the next"
    }
}

var k5min:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "5 دقائق"
    }
    else
    {
        return "5 min"
    }
}

var kEnterPin:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أدخل دبوس"
    }
    else
    {
        return "Enter Pin"
    }
}
var kOtpNotMatch:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Otp غير متطابق"
    }
    else
    {
        return "Otp does not match"
    }
}
var kapiError:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "خطأ"
    }
    else
    {
        return "ERROR"
    }
}
var kCreate:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "خلق"
    }
    else
    {
        return "Create"
    }
}
var kYourAccount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الحساب الخاص بك"
    }
    else
    {
        return "Your Account"
    }
}
var kFirstname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم الاول"
    }
    else
    {
        return "First name"
    }
}
var kLastname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الكنية"
    }
    else
    {
        return "Last name"
    }
}
var kBirthday:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عيد الميلاد"
    }
    else
    {
        return "Birthday"
    }
}
var kGender:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "جنس"
    }
    else
    {
        return "Gender"
    }
}
var kMale:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الذكر"
    }
    else
    {
        return "Male"
    }
}
var kFemale:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إناثا"
    }
    else
    {
        return "Female"
    }
}
var kTermsDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لقد قرأت ووافقت على"
    }
    else
    {
        return "I HAVE READ AND AGREED TO THE"
    }
}
var kTermsCondition:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بالانضمام إلى برنامج مكافآت النجوم ، وافقت على استخدام بياناتي ومشاركتها كما هو موضح في البنود والشروط الخاصة بمكافآت النجوم ، وقد تتم مشاركة بياناتي وأنني قد أتلقى اتصالات تسويقية من شركاء شركة Starco International لكسب واسترداد (وشركات مجموعتهم. ) والأطراف الثالثة الأخرى التي تساعد المسؤول برنامج المكافآت نجم."
    }
    else
    {
        return "By joining the star rewards program, I agreed that my data will be used and shared as described in star rewards Terms and Conditions my data may be shared and that I may receive marketing communications from starco international company earning and redemptions partners (and their group companies) and other third parties who helps adminiser the star reward programme."
    }
}

var kalertFirstName:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء ادخال الاسم"
    }
    else
    {
        return "Please enter first name"
    }
}
var kAlertLastname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال الاسم الأخير"
    }
    else
    {
        return "Please enter last name"
    }
}
var kAlertDob:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى تحديد تاريخ الميلاد"
    }
    else
    {
        return "Please select birth date"
    }
}
var kAlertGender:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى تحديد الجنس"
    }
    else
    {
        return "Please select gender"
    }
}
var kAlertterms:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى الموافقة على البنود والشروط"
    }
    else
    {
        return "Please agree to our terms and conditions"
    }
}
var kAssent:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "موافقة"
    }
    else
    {
        return "Assent"
    }
}
var kSuccessAcount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم تفعيل حسابك بنجاح !"
    }
    else
    {
        return "Your account has been activated successfully !"
    }
}
var kChooseBrand:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر العلامة التجارية التي تهتم بها:"
    }
    else
    {
        return "Choose the brand you're interested in:"
    }
}
var kBrandAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر على الأقل ماركة واحدة"
    }
    else
    {
        return "Choose atleast one brand"
    }
}
var khome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الصفحة الرئيسية"
    }
    else
    {
        return "Home"
    }
}
var kRewards:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المكافآت"
    }
    else
    {
        return "Rewards"
    }
}
var kMall:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مجمع تجاري"
    }
    else
    {
        return "Mall"
    }
}
var kNews:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أخبار"
    }
    else
    {
        return "News"
    }
}
var kMere:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أكثر من"
    }
    else
    {
        return "More"
    }
}
var kworth:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يستحق"
    }
    else
    {
        return "Worth"
    }
}
var kYourRewards:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المكافآت الخاصة بك"
    }
    else
    {
        return "Your Rewards"
    }
}
var kCompletePopupDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إكمال ملفك الشخصي فتح الفداء"
    }
    else
    {
        return "Complete your profile to UNLOCK redemption"
    }
}

var kunlock:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "فتح"
    }
    else
    {
        return "UNLOCK"
    }
}

var kTransactions:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المعاملات"
    }
    else
    {
        return "Transactions"
    }
}

var kworthPointEarn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يستحق نقطة المكتسبة"
    }
    else
    {
        return "worth Point Earned"
    }
}

var kHowToEarn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كيف تكسب نقطة!"
    }
    else
    {
        return "How do you earn point!"
    }
}

var kShopping:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التسوق"
    }
    else
    {
        return "Shopping"
    }
}

var kTellFrnds:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أخبر أصدقائك"
    }
    else
    {
        return "Tell your friends"
    }
}
var kCompleteProfile:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أكمل ملفك الشخصي"
    }
    else
    {
        return "Complete your profile"
    }
}
var kBrand:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العلامات التجارية"
    }
    else
    {
        return "BRANDS"
    }
}
var kStore:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "متجر"
    }
    else
    {
        return "STORES"
    }
}
var kCountry:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بلد"
    }
    else
    {
        return "Country"
    }
}
var kCity:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مدينة"
    }
    else
    {
        return "City"
    }
}
var kHomeAddress:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عنوان المنزل"
    }
    else
    {
        return "Home address"
    }
}

var kSelCountry:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى اختيار البلد"
    }
    else
    {
        return "Please select country"
    }
}
var kSelCity:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى اختيار المدينة"
    }
    else
    {
        return "Please select city"
    }
}
var kenterAdd:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "من فضلك ادخل العنوان"
    }
    else
    {
        return "Please enter address"
    }
}
var keSelLang:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء اختيار لغتك"
    }
    else
    {
        return "Please select your language"
    }
}
var kupdate:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تحديث"
    }
    else
    {
        return "Update"
    }
}
var kCamera:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الة تصوير"
    }
    else
    {
        return "Camera"
    }
}

var kGallary:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "صور و"
    }
    else
    {
        return "Gallary"
    }
}
var kOffers:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عروض"
    }
    else
    {
        return "Offers"
    }
}
var kNationality:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "جنسية"
    }
    else
    {
        return "Nationality"
    }
}
var kRedeem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "خلص"
    }
    else
    {
        return "Redeem"
    }
}
var kInformation:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Information"
    }
    else
    {
        return "Information"
    }
}
var kCollection:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Collection"
    }
    else
    {
        return "Collection"
    }
}
var kYourPoints:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Your Points"
    }
    else
    {
        return "Your Points"
    }
}
var kYourLevel:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Your Level"
    }
    else
    {
        return "Your Level"
    }
}
var kOnattendingEvents:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "On attending Events"
    }
    else
    {
        return "On attending Events"
    }
}
var kPoints:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "Point"
    }
    else
    {
        return "Point"
    }
}
