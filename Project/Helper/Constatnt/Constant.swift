//
//  Constrant.swift
//  Cosmos
//
//  Created by MAC on 1/28/18.
//  Copyright © 2018 cosmoskw. All rights reserved.
//

import Foundation


struct GlobalConstants
{
//    static let kBaseURL = "http://starco.innovasolution.net/"
    
    static let kBaseURL = "http://62.215.166.23/"

    static let kisArabic = UserDefaults.standard.bool(forKey: "arabic")
    
//    static let lightTheme = UIColor(red: 163.0/255.0, green: 45.0/255.0, blue: 119.0/255.0, alpha: 1)
    
    static let DarkTheme = UIColor(red: 74.0/255.0, green: 33.0/255.0, blue: 84.0/255.0, alpha: 1)

    
    static let lightTheme = UIColor(red: 231.0/255.0, green: 56.0/255.0, blue: 146.0/255.0, alpha: 1)

    static let kEnglishRegular = "Benetton-Regular"
    static let kEnglishBold = "Benetton-Stencil"
    static let kEnglishLight = "Benetton-Light"

    
    static let kArabicRegular = "Benetton-Regular"
    static let kArabicBold = "Benetton-Stencil"
    static let kArabicLight = "Benetton-Light"

//    static let kArabicLight = "Helvetica Neue"
//    static let kArabicRegular = "Helvetica Neue"
//    static let kArabicBold = "Helvetica Neue"
    
    static let kStoreKey = "X-Starco-SecurityToken"
    static let kHeaderKey = "X-Starco-SecurityToken"

    static let kisFirstTime = "XIsATVFirstTime"
    static let kisLogin = "isLogin"

    static let wRegisterDevice = "RegisterDevice"
    static let wRegisterMobile = "RegisterMobile"
    static let wAuthenticateUser = "AuthenticateUser"
    static let wGetCountries = "GetCountries"
    static let wGetAreas = "GetAreas"
    static let wGetBrands = "GetBrands"
    static let wGetAdvertisements = "GetAdvertisements"
    static let wGetLocations = "GetLocations"
    static let wAddFavoriteBrands = "AddFavoriteBrands"
    static let wRemoveFavoriteBrand = "RemoveFavoriteBrand"
    static let wGetFavoriteItems = "GetFavoriteItems"
    static let wGetBrandDetail  = "GetBrandDetail"
    static let wUpdateProfile = "UpdateProfile"
    static let wGetUserProfile =  "GetUserProfile"
    static let wGetRewards = "GetRewards"
    static let wGetTransactions = "GetTransactions"
    static let wGetNews = "GetNews"
    static let wGetNewsDetail = "GetNewsDetail"
    static let wGetCMSContent = "GetCMSContent"
    static let wLogout = "Logout"
    static let wGetNationalities = "GetNationalities"
    static let wGetBarcode = "GetBarcode"
    static let wGetNotifications = "GetNotifications"
    static let wGetOffers = "GetOffers"
    static let wScanEvent = "ScanEvent"


    
    static let kKNETUrl = "https://pantoneme.com/PantoneKnet.aspx?orderId="
    static let kPaymentSuccessUrl1 = "https://pantoneme.com/Receipt.aspx"
    static let kPaymentSuccessUrl = "Receipt.aspx"
    static let kPaymentFailureUrl  = "Error.aspx"

}



