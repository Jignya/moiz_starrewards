//
//  Manager.swift
//  Maki
//   Change check
//  Created by Jigar Joshi on 7/27/16.
//  Copyright © 2016 olivermaki. All rights reserved.
//

import UIKit
import Foundation

class Manager: NSObject
{
    static let sharedInstance = Manager()
    var sidebar = SideBarVC()
    var CountryPopup = CustomCountryPopup()
    var tabbarController = CustomTabbarVC()

    var arrcountry : NSMutableArray = []
    var arrArea : NSMutableArray = []
    var dictprofile : NSMutableDictionary = [:]

    var CartCount : String!
    var worthPoint : String!
    var earnPoint : String!

    var Slidetime : Int = 3

    var memberLevel : NSNumber!

    
    //MARK: get comma separeted string
    func getCommaSeparatedValuefromArray(_ array: NSMutableArray, forkey strKey: String) -> String {
        let array = array
        var strSepratedKey = ""
        
        for i in 0..<(array.count)
        {
            var dict = array[i] as? [AnyHashable : Any]
            strSepratedKey = strSepratedKey + (", \(dict?[strKey ] ?? "")")
        }
        if strSepratedKey.hasPrefix(",") && strSepratedKey.count > 1
        {
            strSepratedKey = (strSepratedKey as? NSString)!.substring(from: 1)
        }
        
        return strSepratedKey
    }
    
    
    func arrFilterDataforstring(_ str1: String?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        let arrMain = arrMain
        let predicate = NSPredicate(format: "self.%@ LIKE[c] %@", str2 ?? "", str1 ?? "")
        var arr1: [AnyHashable]? = nil
        if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
            arr1 = filtered as? [AnyHashable]
        }
        
        return NSMutableArray(array: (arr1 as? Array)!)
    }
    
    func arrFilterDataforKey(_ str1: String?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        let arrMain = arrMain
        let predicate = NSPredicate(format: "self.%@ = %@", str2 ?? "" , str1 ?? "")
        var arr1: [AnyHashable]? = nil
        if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
            arr1 = filtered as? [AnyHashable]
        }
        
        return NSMutableArray(array: (arr1 as? Array)!)
    }
    
    func arrFilterDataforKey1(_ str1: String?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        if str1?.isNumeric == true
        {
            var myNumber : NSNumber!
            if let myInteger = Int(str1!) {
                myNumber = NSNumber(value:myInteger)
            }
            
            let arrMain = arrMain
            let predicate = NSPredicate(format: "self.%@ = %@", str2 ?? "" , myNumber ?? "")
            var arr1: [AnyHashable]? = nil
            if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
                arr1 = filtered as? [AnyHashable]
            }
            
            return NSMutableArray(array: (arr1 as? Array)!)
            
        }
        else
        {
            let arrMain = arrMain
            let predicate = NSPredicate(format: "self.%@ = 96", str2 ?? "" , str1 ?? "")
            var arr1: [AnyHashable]? = nil
            if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
                arr1 = filtered as? [AnyHashable]
            }
            
            return NSMutableArray(array: (arr1 as? Array)!)
        }
        
    }
    
    //MARK: set In text file
    func SaveDatainTextFile(fileName:String , arrData:NSMutableArray)
    {
        var jsonData: NSData!
        do
        {
            jsonData = try JSONSerialization.data(withJSONObject: arrData, options: JSONSerialization.WritingOptions()) as NSData
        }
        catch let error as NSError
        {
            print("Array to JSON conversion failed: \(error.localizedDescription)")
        }
        let file = fileName
        let writePath = (self.documentsDirectory() as NSString).appendingPathComponent(file)
        jsonData!.write(toFile: writePath, atomically:true)
    }
    
    
    func getSavedDataFromTextFile(fileName:String) -> NSMutableArray
    {
        let file = fileName
        
        var dataarray : NSMutableArray = []
        
        if let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).appendingPathComponent(file)
            do {
                let text2 = try String(contentsOf: path!, encoding: .utf8)
                
                let data = text2.data(using: String.Encoding.utf8)
                
               let jsonArray = try JSONSerialization.jsonObject(with:data!, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                
//                print("get Array \(jsonArray)")

                dataarray = NSMutableArray(array: jsonArray)

            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        return dataarray
    }
    
    func DeleteSavedDataFromTextFile(fileName:String)
    {
        let file = fileName
        
        var filePath = ""
        
        
        let dirs : [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        
        if dirs.count > 0
        {
            let dir = dirs[0] //documents directory
            filePath = dir.appendingFormat("/" + file)
            print("Local path = \(filePath)")
            
        } else {
            print("Could not find local directory to store file")
            return
        }
        
        
        do {
            
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: filePath)
            {
                // Delete file
                try fileManager.removeItem(atPath: filePath)
            }
            else
            {
                print("File does not exist")
            }
            
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        
    }
    
    func documentsDirectory() -> String
    {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        return documentsFolderPath
    }
    
    func StrikeThroughString(str:String) -> NSAttributedString
    {
        let attributedString = NSMutableAttributedString(string: str)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
        
        return attributedString

    }
    
    //MARK:- add tabbar
    
    func addTabBar1(_ controller: UIViewController?,tab:String)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        let view: UIView? = window?.viewWithTag(111)
        
        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        
        
        //        if view == nil
        //        {
        tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC", bundle: nil)
        tabbarController.view.tag = 111
        controller?.view.addSubview(tabbarController.view)
        if let aWidth = window?.frame.size.width
        {
            tabbarController.view.frame = CGRect(x: 0, y: (window?.frame.size.height)! - tabbarController.view.frame.size.height - bottomPadding, width: aWidth, height: tabbarController.view.frame.size.height)
        }
        //        }
        
        tabbarController.btnHome.isSelected = false
        tabbarController.btnReward.isSelected = false
        tabbarController.btnMall.isSelected = false
        tabbarController.btnNews.isSelected = false
        tabbarController.btnMore.isSelected = false
        
        tabbarController.btnHome1.isSelected = false
        tabbarController.btnReward1.isSelected = false
        tabbarController.btnMall1.isSelected = false
        tabbarController.btnNews1.isSelected = false
        tabbarController.btnMore1.isSelected = false
        
        if tab == "0"
        {
            tabbarController.btnHome.isSelected = true
            tabbarController.btnHome1.isSelected = true
        }
        else if tab == "1"
        {
            tabbarController.btnReward.isSelected = true
            tabbarController.btnReward1.isSelected = true
        }
        else if tab == "2"
        {
            tabbarController.btnMall.isSelected = true
            tabbarController.btnMall1.isSelected = true
        }
        else if tab == "3"
        {
            tabbarController.btnNews.isSelected = true
            tabbarController.btnNews1.isSelected = true
        }
        else if tab == "4"
        {
            tabbarController.btnMore.isSelected = true
            tabbarController.btnMore1.isSelected = true
        }
        
        
        
    }
    
    func addTabBar(_ controller: UIViewController?,tab:String)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        //        let view: UIView? = window?.viewWithTag(101)
        
        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        
        
        //        if view == nil
        //        {
        tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC", bundle: nil)
        tabbarController.view.tag = 111
        window!.addSubview(tabbarController.view)
        if let aWidth = window?.frame.size.width
        {
            tabbarController.view.frame = CGRect(x: 0, y: (window?.frame.size.height)! - tabbarController.view.frame.size.height - bottomPadding, width: aWidth, height: tabbarController.view.frame.size.height)
        }
        //        }
        
        tabbarController.btnHome.isSelected = false
        tabbarController.btnReward.isSelected = false
        tabbarController.btnMall.isSelected = false
        tabbarController.btnNews.isSelected = false
        tabbarController.btnMore.isSelected = false
        
        tabbarController.btnHome1.isSelected = false
        tabbarController.btnReward1.isSelected = false
        tabbarController.btnMall1.isSelected = false
        tabbarController.btnNews1.isSelected = false
        tabbarController.btnMore1.isSelected = false
        
        if tab == "0"
        {
            tabbarController.btnHome.isSelected = true
            tabbarController.btnHome1.isSelected = true
        }
        else if tab == "1"
        {
            tabbarController.btnReward.isSelected = true
            tabbarController.btnReward1.isSelected = true
        }
        else if tab == "2"
        {
            tabbarController.btnMall.isSelected = true
            tabbarController.btnMall1.isSelected = true
        }
        else if tab == "3"
        {
            tabbarController.btnNews.isSelected = true
            tabbarController.btnNews1.isSelected = true
        }
        else if tab == "4"
        {
            tabbarController.btnMore.isSelected = true
            tabbarController.btnMore1.isSelected = true
        }
        
        
    }
    
    
    func removeTabbar()
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        
        if ((window?.viewWithTag(111)) != nil)
        {
            let vc = window?.viewWithTag(111)
            vc?.removeFromSuperview()
        }
        
        tabbarController.view.removeFromSuperview()
        
    }
    
    func hideTabBar()
    {
        tabbarController.view.isHidden = true
    }
    
    func showTabBar() {
        tabbarController.view.isHidden = false
    }
    
    func bringTabbarToFront()
    {
        tabbarController.view.superview?.bringSubviewToFront(tabbarController.view)
    }
    
    //MARK:- add popup
    
    func addCustomPopup(_ completion: @escaping (_ dictSelect: NSDictionary) -> Void ,arrData:NSMutableArray,isLanguage:Bool ,isNationality:Bool , isCategories:Bool ,isProfession:Bool ,isother:Bool,_ controller: UIViewController?)
    {
        if arrData.count > 0
        {
            var Storyboard = UIStoryboard()
            
            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                Storyboard = UIStoryboard(name: "Main_Ar", bundle: nil)
            }
            else
            {
                Storyboard = UIStoryboard(name: "Main", bundle: nil)
                
            }
            
            CountryPopup = Storyboard.instantiateViewController(withIdentifier: "CustomCountryPopup") as! CustomCountryPopup
            
            
            controller?.view.addSubview(CountryPopup.view)
            
            
            CountryPopup.openView({ (dictSelect) in
                completion(dictSelect)
                
            }, arrData: arrData, isLanguage: isLanguage, isNationality: isNationality, isCategories: isCategories, isProfession: isProfession, isother: isother)
        }
    }
    
    //MARK:- add sidebar
    
    func addSideBar(controller : UIViewController)
    {
        //        let Storyboard = UIStoryboard(name: (GlobalConstants.kisArabic) ? "Main_ar" : "Main", bundle: nil)
        var Storyboard = UIStoryboard()
        
        if UserDefaults.standard.bool(forKey: "arabic") == true
        {
            Storyboard = UIStoryboard(name: "Main_ar", bundle: nil)
        }
        else
        {
            Storyboard = UIStoryboard(name: "Main", bundle: nil)
            
        }
        sidebar = Storyboard.instantiateViewController(withIdentifier: "SideBarVC") as! SideBarVC
        
        sidebar.view.tag = 2226
        
//        if controller is HomeVC
//        {
//            sidebar.strcomeFrom = "home"
//        }
//        else
//        {
//            sidebar.strcomeFrom = "other"
//        }
        
        controller.view.addSubview(sidebar.view)
        controller.addChild(sidebar)
        sidebar.view.layoutIfNeeded()
    
        
//        if UserDefaults.standard.bool(forKey: "arabic") == false
//        {
//            sidebar.viewTable.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 - 10, height: UIScreen.main.bounds.size.height)
//
//        }
//        else
//        {
//            sidebar.viewTable.frame=CGRect(x: UIScreen.main.bounds.size.width, y: sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 - 10, height: UIScreen.main.bounds.size.height);
//
//        }
//
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//
//            if UserDefaults.standard.bool(forKey: "arabic") == false
//            {
//
//                self.sidebar.viewTable.frame=CGRect(x: 0, y: self.sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 + 35, height: self.sidebar.viewTable.frame.size.height);
//            }
//            else
//            {
//                self.sidebar.viewTable.frame=CGRect(x: UIScreen.main.bounds.size.width/2 - 35, y: self.sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 + 35, height: self.sidebar.viewTable.frame.size.height);
//
//            }
        

            
//        }  , completion:nil)
        
    }
    
    
    func removesidemenu()
    {
//        sidebar.Hidesidemenu()
    }
    
    
    func showValidation(msg:String , Bgcolor:UIColor , textcolor:UIColor)
    {
        let snackbar = TTGSnackbar(message: msg, duration: .short)
        snackbar.backgroundColor = Bgcolor
        snackbar.tintColor = textcolor
        if UserDefaults.standard.bool(forKey: "arabic") == true
        {
            snackbar.messageTextAlign = .right
            
        }
        else
        {
            snackbar.messageTextAlign = .left
        }
        snackbar.cornerRadius = 0
        snackbar.show()
    }
    
    //MARK: function Set font
    func setFontFamily(_ fontFamily: String, for view: UIView, andSubViews isSubViews: Bool)
    {
        if UserDefaults.standard.bool(forKey: "arabic") == false
        {
            if (view is UILabel)
            {
                let lbl = view as? UILabel
                if lbl?.tag == 25
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishBold, size: (lbl?.font.pointSize)!) {
                        lbl?.font = aSize
                    }
                }
                else if lbl?.tag == 26
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishLight, size: (lbl?.font.pointSize)!) {
                        lbl?.font = aSize
                    }
                }
                else
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishRegular, size: ((lbl?.font.pointSize)! + 2)) {
                        lbl?.font = aSize
                    }
                }
            }
            if (view is UITextView)
            {
                let txt = view as? UITextView
                
                if let aSize = txt?.font?.pointSize
                {
                    txt?.font = UIFont(name: GlobalConstants.kEnglishRegular, size: aSize)
                }
                
            }
            
            if (view is UITextField)
            {
                let txt1 = view as? UITextField
                
                if let aSize = txt1?.font?.pointSize
                {
                    txt1?.font = UIFont(name: GlobalConstants.kEnglishRegular, size: aSize)
                }
            }
            if (view is UIButton)
            {
                
            }
            
            if (view is UIImageView)
            {
                let imgview = view as? UIImageView
                
                if imgview?.tag == 15
                {
                    imgview?.image = imgview?.image!.withRenderingMode(.alwaysTemplate)
                    imgview?.tintColor = UIColor.darkGray

                }
                
            }
           
            if isSubViews
            {
                for sview: UIView in view.subviews
                {
                    setFontFamily(fontFamily, for: sview, andSubViews: true)
                }
            }
        }
        else
        {
            if (view is UILabel)
            {
                let lbl = view as? UILabel
                if lbl?.tag == 25
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicBold, size: (lbl?.font.pointSize)!) {
                        lbl?.font = aSize
                    }
                }
                else if lbl?.tag == 26
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicLight, size: (lbl?.font.pointSize)!) {
                        lbl?.font = aSize
                    }
                }
                else
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicRegular, size: ((lbl?.font.pointSize)!))
                    {
                        lbl?.font = aSize
                    }
                }
            }
            if (view is UITextView) {
                let txt = view as? UITextView

                if let aSize = txt?.font?.pointSize
                {
                    txt?.font = UIFont(name:GlobalConstants.kArabicRegular, size: aSize)
                }
            }
            if (view is UITextField)
            {
                let txt1 = view as? UITextField
                if let aSize = txt1?.font?.pointSize
                {
                    txt1?.font = UIFont(name: GlobalConstants.kArabicRegular, size: aSize)
                }
            }
            if (view is UIImageView)
            {
                let imgview = view as? UIImageView
                
                if imgview?.tag == 15
                {
                    imgview?.image = imgview?.image!.withRenderingMode(.alwaysTemplate)
                    imgview?.tintColor = UIColor.darkGray
                    
                }
                
            }
            if isSubViews
            {
                for sview: UIView in view.subviews {
                    setFontFamily(fontFamily, for: sview, andSubViews: true)
                }
            }
            
        }
        
    }
    
    //MARK:- Version check
    func newVersionPresent() -> Bool
    {
        let infoDict = Bundle.main.infoDictionary
        let appID = infoDict?["CFBundleIdentifier"] as? String
        let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(appID ?? "")")
        var data: Data? = nil
        
        if let anUrl = url
        {
            do {
                try data = Data(contentsOf: anUrl)
            }
            catch _ {
                // Error handling
            }
            
        }
        var itunesVersionInfo : NSDictionary!
        if let aData = data
        {
            itunesVersionInfo = try! JSONSerialization.jsonObject(with: aData, options: []) as! NSDictionary
        }
        if itunesVersionInfo != nil
        {
            let getArr = itunesVersionInfo["results"] as! NSArray
            
            if itunesVersionInfo["resultCount"] as? Int == 1
            {
                let arrAppStore = getArr.object(at: 0) as! Dictionary<AnyHashable, Any>
                let appStoreVersion =  String(format: "%@", arrAppStore["version"] as! CVarArg)
                let currentVersion = String(format: "%@", infoDict?["CFBundleShortVersionString"] as! CVarArg)
                if appStoreVersion.compare(currentVersion, options: .numeric, range: nil, locale: .current) == .orderedDescending
                {
                    return true
                }
            }
        }
        
        
        return false
    }
    
    //MARK:- area webservice
    
    func wscallforArea()
    {
        let countryId = String(format: "%@", (UserDefaults.standard.value(forKey: "country_id") as? NSNumber)!)
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetAreas,countryId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")

        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in

            DispatchQueue.main.async
            {
                self.arrArea = NSMutableArray(array: result["Areas"] as! Array)
            }


        }

    }
    
    
    func wscallforCountries()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetCountries,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")

        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in

            DispatchQueue.main.async
            {
                self.arrcountry = NSMutableArray(array: result["Countries"] as! Array)
            }


        }

    }
    
    
    func wscallforCustomerRewards()
    {
        let userId = UserDefaults.standard.value(forKey: "userId") as! String

        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetRewards,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)

                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.worthPoint = String(format: "%.2f", (result["Rewards_Worth"] as? NSNumber)!.floatValue)
                        self.earnPoint = String(format: "%.2f", (result["Points_Earned"] as? NSNumber)!.floatValue)
                        self.memberLevel = result["Member_Level"] as! NSNumber

                        
                        
                        let vc = UIApplication.topViewController() as! UIViewController
                        if (vc.isKind(of: HomeVC.self))
                        {
                            let vc1 = vc as? HomeVC
                            vc1?.viewWillAppear(true)
                            
                        }
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    
    func displayAddress(_ dict: NSMutableDictionary) -> String
    {
        let dict1 = dict.mutableCopy() as! NSMutableDictionary
        var strdata = ""
        
        let array = dict.allKeys(for: "")
        for i in 0..<(array.count ?? 0)
        {
            dict1.removeObject(forKey: array[i])
        }
        if dict1["AddressName"] != nil
        {
            if let aKey = dict1["AddressName"] {
                strdata = strdata + ("\(aKey)") + "\n"
            }
        }
        if dict1["AREA_NAME"] != nil {
            if let aKey = dict1["AREA_NAME"] {
                strdata = strdata + kArea + " : " + ("\(aKey)")
            }
        }
        if dict1["Block"] != nil {
            if let aKey = dict1["Block"] {
                strdata = strdata + " ," + kblock + " : " + ("\(aKey)")
            }
        }
        if dict1["Street"] != nil {
            if let aKey = dict1["Street"] {
                strdata = strdata + " ," + kStreet + " : " + ("\(aKey)")
            }
        }
       
        if dict1["HouseNo"] != nil {
            if let aKey = dict1["HouseNo"] {
                strdata = strdata + " ," + kHousNo + " : " + ("\(aKey)")
            }
        }
        if dict1["Avenue"] != nil {
            if let aKey = dict1["Avenue"] {
                strdata = strdata + " ," + kAvenue + " : " + ("\(aKey)")
            }
        }
        if dict1["Building"] != nil {
            if let aKey = dict1["Building"] {
                strdata = strdata + " ," + kBuilding + " : " + ("\(aKey)")
            }
        }
        if dict1["Floor"] != nil {
            if let aKey = dict1["Floor"] {
                strdata = strdata + " ," + kFloor + " : " + ("\(aKey)")
            }
        }
        if dict1["Apparment"] != nil {
            if let aKey = dict1["Apparment"] {
                strdata = strdata + " ," + kAppartment + " : " + ("\(aKey)")
            }
        }
//        if dict1["mobile_number"] != nil {
//            if let aKey = dict1["mobile_number"] {
//                strdata = strdata + ("\(aKey)")
//            }
//        }
        if dict1["ExtraDirection"] != nil {
            if let aKey = dict1["ExtraDirection"] {
                strdata = strdata + " ," + kExtraDirection + " : " + ("\(aKey)")
            }
        }

        return strdata
    }
    
    
    func displayAddress11(_ dict: NSMutableDictionary) -> String
    {
        let dict1 = dict.mutableCopy() as! NSMutableDictionary
        var strdata = ""
        
        let array = dict.allKeys(for: "")
        for i in 0..<(array.count ?? 0)
        {
            dict1.removeObject(forKey: array[i])
        }
        if dict1["AREA_NAME"] != nil {
            if let aKey = dict1["AREA_NAME"] {
                strdata = strdata + kArea + " : " + ("\(aKey)\n")
            }
        }
        if dict1["Block"] != nil {
            if let aKey = dict1["Block"] {
                strdata = strdata + " ," + kblock + " : " + ("\(aKey)")
            }
        }
        if dict1["Street"] != nil {
            if let aKey = dict1["Street"] {
                strdata = strdata + " ," + kStreet + " : " + ("\(aKey)")
            }
        }
        
        if dict1["HouseNo"] != nil {
            if let aKey = dict1["HouseNo"] {
                strdata = strdata + " ," + kHousNo + " : " + ("\(aKey)")
            }
        }
        if dict1["Avenue"] != nil {
            if let aKey = dict1["Avenue"] {
                strdata = strdata + " ," + kAvenue + " : " + ("\(aKey)")
            }
        }
        if dict1["Building"] != nil {
            if let aKey = dict1["Building"] {
                strdata = strdata + " ," + kBuilding + " : " + ("\(aKey)")
            }
        }
        if dict1["Floor"] != nil {
            if let aKey = dict1["Floor"] {
                strdata = strdata + " ," + kFloor + " : " + ("\(aKey)")
            }
        }
        if dict1["Apparment"] != nil {
            if let aKey = dict1["Apparment"] {
                strdata = strdata + " ," + kAppartment + " : " + ("\(aKey)")
            }
        }
        if dict1["ExtraDirection"] != nil {
            if let aKey = dict1["ExtraDirection"] {
                strdata = strdata + " ," + kExtraDirection + " : " + ("\(aKey)")
            }
        }
        if dict1["Mobile"] != nil {
            if let aKey = dict1["Mobile"] {
                strdata = strdata + "\n" + kMobNum + " : " + ("\(aKey)")
            }
        }

        
        return strdata
    }
    
    //MARK: Small Alert
    var viewAlert:UIView!

    func showSmallAlert(_ strText: String?)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        let intWidth: Int = ((strText?.count ?? 0) > 17) ? ((strText?.count ?? 0) > 30) ? 280 : 220 : 150
        if window?.viewWithTag(101) != nil
        {
            self.hideViewandCompletion({ complete in
            })

        }
        
        viewAlert = UIView(frame: CGRect(x: ((window?.frame.size.width ?? 0.0) - CGFloat(intWidth)) / 2, y: (window?.frame.size.height ?? 0.0) - 130, width: CGFloat(intWidth), height: 40))
        viewAlert.backgroundColor = UIColor(red: 30.0 / 255.0, green: 181.0 / 255.0, blue: 180.0 / 255.0, alpha: 0.9)
        viewAlert.layer.cornerRadius = 10
        viewAlert.clipsToBounds = true
        viewAlert.tag = 101
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: viewAlert.frame.size.width, height: viewAlert.frame.size.height))
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        lbl.text = strText
        lbl.font = UIFont(name: "Calibri", size: 16)
        viewAlert.addSubview(lbl)
        window?.addSubview(viewAlert)
//        view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        viewAlert.alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.viewAlert.alpha = 1.0
//            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }) { finished in
//            UIView.animate(withDuration: 0.1, animations: {
//                self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
//            }) { finished in
//                UIView.animate(withDuration: 0.1, animations: {
//                    self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//                }) { finished in
//                    RunLoop.current.run(until: Date(timeIntervalSinceNow: 1.0))
                    sleep(UInt32(2.0))
                    self.hideViewandCompletion({ complete in
                    })
//                }
//            }
        }
    }
    
    func hideViewandCompletion(_ complete: @escaping (_ complete: Bool) -> Void)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!

        if ((window?.viewWithTag(101)) != nil)
        {
            viewAlert.alpha = 0.0
            viewAlert.removeFromSuperview()

        }
        complete(true)

        
//        UIView.animate(withDuration: 0.1, animations: {
//            self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
//        }) { finished in
//            UIView.animate(withDuration: 0.2, animations: {
//                self.view.alpha = 0.0
//                self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//                 self.view.removeFromSuperview()

//            }) { finished in
//                self.view.removeFromSuperview()
//                complete(true)
//            }
        
//        }
    }
}

extension Collection where Iterator.Element == [String:Any] {
    func toJSONStringFilter(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:Any]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}


