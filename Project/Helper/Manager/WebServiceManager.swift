//
//  WebServiceManager.swift
//  Benihana
//
//  Created by Monali on 7/11/16.
//  Copyright © 2016 Monali. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServiceManager: NSObject
{
    class var sharedInstance: WebServiceManager {
        struct Static {
            static let instance: WebServiceManager = WebServiceManager()
        }
        return Static.instance
    }
    
    // MARK:- Alamofire
    
    func postWebServiceWithoutHeaderAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let myUrl = URL(string: strurl)
        
        Alamofire.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                
                return .success
            }
            .responseJSON { response in
                debugPrint(response)
                if (response.error) == nil
                {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    completion(swiftyJsonVar.dictionaryObject! as NSDictionary)
                }
                else
                {
                    debugPrint(response.error)

                }
                
                if isshowLoading
                {
                  Helper.sharedInstance.DismissProgressBar()
                }
        }
    }
    
    func postWebServiceWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
        {
            if isshowLoading
            {
                Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
            }
        }

        let myUrl = URL(string: strurl)
        
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        
        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
//                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                
                return .success
            }
            .responseJSON { response in
//                debugPrint(response)
                if (response.error) == nil
                {
                let swiftyJsonVar = JSON(response.result.value!)
                print(swiftyJsonVar)
                completion(swiftyJsonVar.dictionaryObject as! NSDictionary)
                   
                }
                else
                {
                    debugPrint(response.error)
                   

                }
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
                

        }
    }
    
    func getWebServiceWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String

        let myUrl = URL(string: strurl)
        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(myUrl!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                
                return .success
            }
            .responseJSON { response in
//                debugPrint(response)
                if (response.error) == nil
                {
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    completion(self.nullToNil(value: swiftyJsonVar.dictionaryObject as AnyObject) as! NSDictionary)
                }
                else
                {
                    debugPrint(response.error)
                    
                }
                
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
        }
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject?
    {
        if value is NSNull
        {
            return nil
        }
        else
        {
            return value
        }
    }
    
    func requestWithImage(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = URL(string: endUrl)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
        
    
    
    // MARK:--------------------

   
    
    fileprivate func post(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "POST", completion: completion)
    }
    
    fileprivate func put(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "PUT", completion: completion)
    }
    
    fileprivate func get(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "GET", completion: completion)
    }
    
    fileprivate func dataTask(_ request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, json as AnyObject)
                } else {
                    completion(false, json as AnyObject)
                }
            }
        }) .resume()
    }
    
    
    fileprivate func clientURLRequest(_ path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        //        let request = NSMutableURLRequest(URL: NSURL(string: "http://api.example.com/"+path)!)
        let request = NSMutableURLRequest(url: URL(string: String(format:"%@%@",GlobalConstants.kBaseURL,path))!)
        
        if let params = params {
            var paramString = ""
            for (key, value) in params {
                let escapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let escapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                paramString += "\(escapedKey)=\(escapedValue)&"
            }
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = paramString.data(using: String.Encoding.utf8)
        }
        
        return request
    }
    
    
    func postWebServiceForOcrImageParseWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let myUrl = URL(string: strurl)
        
        let headers: HTTPHeaders = [
            "apikey": "7fdfe9c8ed88957",
        ]
//        "Content-Type": "application/json"

        
        Alamofire.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                
                return .success
            }
            .responseJSON { response in
                //                debugPrint(response)
                if (response.error) == nil
                {
                    let swiftyJsonVar = JSON(response.result.value!)
                    print(swiftyJsonVar)
                    completion(swiftyJsonVar.dictionaryObject as! NSDictionary)
                    
                }
                else
                {
                    debugPrint(response.error)
                    
                    
                }
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
                
                
        }
    }
    
    
}

