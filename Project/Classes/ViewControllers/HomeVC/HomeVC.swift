//
//  HomeVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout


class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate,FSPagerViewDelegate,FSPagerViewDataSource
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionAdv: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viewoffer: UIView!
    @IBOutlet weak var lblRewards: UILabel!
    @IBOutlet weak var lblWorth: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var imgMember: UIImageView!

    @IBOutlet weak var btnoffer: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var pagerview: FSPagerView!{
        didSet {
            self.pagerview.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    @IBOutlet weak var pagerview1: FSPagerView!{
        didSet {
            self.pagerview1.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell1")
        }
    }

    
    

    @IBOutlet weak var conColAdvHeight: NSLayoutConstraint!
    @IBOutlet weak var conPagerviewHeight: NSLayoutConstraint!


    var arrayAdv : NSMutableArray = []
    var arrayNews : NSMutableArray = []

    var timer = Timer()
    let layout = AnimatedCollectionViewLayout()
    
    var indexRow : Int = 0
    var strcomeFrom : String = ""
    var strBarcode : String!
    var Duration : Int!
    var strExpiryDate : String!
    


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        pageControl.isHidden = true
        
        pagerview.delegate = self
        pagerview.dataSource = self
        pagerview.itemSize = CGSize(width: self.view.frame.size.width - 70, height: self.view.frame.size.width - 70)
        pagerview.interitemSpacing = 5
        pagerview.transformer = FSPagerViewTransformer(type: .linear)
//        pagerview.decelerationDistance = 2
        pagerview.isInfinite = true
        pagerview.automaticSlidingInterval = 3.0

        
        pagerview1.delegate = self
        pagerview1.dataSource = self
        pagerview1.itemSize = CGSize(width: self.view.frame.size.width - 70, height: 150)
        pagerview1.interitemSpacing = 5
        pagerview1.transformer = FSPagerViewTransformer(type: .linear)
        pagerview1.isInfinite = true

        
        self.btnPoints.isHidden = false
        btnPoints.setTitle(kRedeem.uppercased(), for: .normal)
        
//        self.conColAdvHeight.constant = (5 * self.view.frame.size.width) / 4
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 50.0)
            let leftAnimation = AnimationType.from(direction: .left, offSet: 50.0)
            
            viewoffer.animate(withType: [bottomAnimation, leftAnimation])
            
            pageControl.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        else
        {
            let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 50.0)
            let leftAnimation = AnimationType.from(direction: .right, offSet: 50.0)
            
            viewoffer.animate(withType: [bottomAnimation, leftAnimation])

        }

        
        lblTitle.text = khome.uppercased()
        lblRewards.text = kRewards.uppercased()
        lblWorth.text = kPoints.uppercased()

        
        pageControl.numberOfPages = 1
        wscallforAdvertise()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        collectionAdv.isScrollEnabled = false
        
        let leftGR = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeHandler_Left(gestureRecognizer:)))
        leftGR.numberOfTouchesRequired = 1
        leftGR.direction = .left
        leftGR.delegate = self
        
        
        let RightGR = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeHandler_Right(gestureRecognizer:)))
        RightGR.numberOfTouchesRequired = 1
        RightGR.direction = .right
        RightGR.delegate = self

        self.collectionAdv.addGestureRecognizer(leftGR)
        self.collectionAdv.addGestureRecognizer(RightGR)
    }
    
    @objc func swipeHandler_Left(gestureRecognizer : UISwipeGestureRecognizer!) {
        if gestureRecognizer.state == .ended {
            // Perform action.
            
            swipeLeft()
        }
    }
    
    @objc func swipeHandler_Right(gestureRecognizer : UISwipeGestureRecognizer!) {
        if gestureRecognizer.state == .ended {
            // Perform action.
            
            swipeRight()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        Manager.sharedInstance.addTabBar1(self, tab: "0")
        
        lblPrice.text = "0.00"
        lblCurrency.text = kCurrency

        if Manager.sharedInstance.worthPoint != nil
        {
            lblPrice.text = Manager.sharedInstance.earnPoint
//            lblCurrency.text = kCurrency
            if Manager.sharedInstance.memberLevel  == 1
            {
                imgMember.image = UIImage(named: "blue.png")
            }
            else if Manager.sharedInstance.memberLevel  == 2
            {
                imgMember.image = UIImage(named: "silver.png")
            }
            else if Manager.sharedInstance.memberLevel  == 3
            {
                imgMember.image = UIImage(named: "gold.png")
            }
            else if Manager.sharedInstance.memberLevel == 4
            {
                imgMember.image = UIImage(named: "platinum.png")
            }
            else if Manager.sharedInstance.memberLevel == 5
            {
                imgMember.image = UIImage(named: "vip.png")
            }
        }
        
        
    }
    
    func RedirectToProfile()
    {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailVC") as! UserDetailVC
        self.navigationController?.pushViewController(login, animated: false)
    }
    
    
    //MARK:- PagerView
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int
    {
        if pagerView == pagerview
        {
           return arrayAdv.count
        }
        else
        {
            return arrayNews.count
        }
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        if pagerView == pagerview
        {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
            
            cell.isHighlighted = false
            let dict = arrayAdv[index] as! NSDictionary
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as! String)
            
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let url = URL(string: urlString!)
            cell.imageView?.setImageWith(url, usingActivityIndicatorStyle: .gray)
            
            //            cell.textLabel?.text = ...
            return cell
        }
        else
        {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell1", at: index)
            
            cell.isHighlighted = false

            let dict = arrayNews[index] as! NSDictionary
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["News_Image_Url"] as! String)
            
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let url = URL(string: urlString!)
            cell.imageView?.setImageWith(url, usingActivityIndicatorStyle: .gray)

            return cell
        }
        
       
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        if pagerView == pagerview
        {
            let dict = NSMutableDictionary(dictionary: arrayAdv[index] as! Dictionary)
            let login = self.storyboard?.instantiateViewController(withIdentifier: "BrandDetailVC") as! BrandDetailVC
            login.BrndId = dict["Brand_Id"] as? NSNumber
            self.navigationController?.pushViewController(login, animated: false)
        }
        else
        {
            let dict = NSMutableDictionary(dictionary: arrayNews[index] as! Dictionary)
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
            detail.newsId = dict["News_Id"] as? NSNumber
            self.navigationController?.pushViewController(detail, animated: false)
            
            
        }
        
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       
        return arrayAdv.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
            let cellReuseIdentifier = "BrandCell"
            let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
            
            let dict = arrayAdv[indexPath.row] as! NSDictionary
        
            cell.lblTitle.text = (dict["Advertisement_Name"] as? String)!.uppercased()

            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as! String)

            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

            let url = URL(string: urlString!)
            cell.imgLogo.setImageWith(url, usingActivityIndicatorStyle: .gray)
        
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: Double(Manager.sharedInstance.Slidetime), target: self, selector: #selector(timerAction), userInfo: nil, repeats:true)
        
            return cell
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        cell.alpha = 0.8
        UIView.animate(withDuration: 0.7) {
            cell.alpha = 1.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:self.view.frame.size.width , height:collectionAdv.frame.size.height)
        
//        pagerview.itemSize = CGSize(width: self.view.frame.size.width - 100, height: collectionAdv.frame.size.height - 100)

        
        return cellSize
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        print("scroll")
    }
   
    @objc func timerAction()
    {
        DispatchQueue.main.async
            {
                if self.indexRow + 1 < self.arrayAdv.count
                {
                    self.indexRow = self.indexRow + 1

                    self.collectionAdv.scrollToItem(at: IndexPath(item:self.indexRow, section: 0), at: .centeredHorizontally, animated: false)
                }
                else
                {
                    self.indexRow = 0
                    self.collectionAdv.scrollToItem(at: IndexPath(item:0, section: 0), at: .centeredHorizontally, animated: false)

                }

        }
        return
        
        
        
        if (collectionAdv.visibleCells.count>0)
        {
            let cell:UICollectionViewCell = (collectionAdv.visibleCells[0])
            
            let index:IndexPath = (collectionAdv!.indexPath(for: cell))!
            
            print(index.row)
            
            if (index.row + 1 < arrayAdv.count) {
                
                collectionAdv.scrollToItem(at: IndexPath(item:index.row + 1, section: 0), at: .centeredHorizontally, animated: true)
                
            }
            else
            {
                collectionAdv.scrollToItem(at: IndexPath(item:0, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    @objc func swipeLeft()
    {
        if collectionAdv.visibleCells.count > 0
        {
            var index: IndexPath? = collectionAdv.indexPathsForVisibleItems.last
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                if (index?.row ?? 0) < arrayAdv.count {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) - 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            else
            {
                if (index?.row ?? 0) + 1 < arrayAdv.count
                {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) + 1, section: 0), at: .centeredHorizontally, animated: false)
                }
            }
            
            
        }
    }
    
    
    @objc func swipeRight()
    {
        if collectionAdv.visibleCells.count > 0 {
            var index: IndexPath? = collectionAdv.indexPathsForVisibleItems.first
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                if (index?.row ?? 0) + 1 < arrayAdv.count
                {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) + 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            else
            {
                if (index?.row ?? 0) < arrayAdv.count {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) - 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = collectionAdv.frame.size.width
        pageControl.currentPage = Int(floor((collectionAdv.contentOffset.x / pageWidth)))
    }
    
    @IBAction func btnOfferClick(_ sender: Any)
    {
       let offer = self.storyboard?.instantiateViewController(withIdentifier: "OfferVC") as! OfferVC
        self.navigationController?.pushViewController(offer, animated: true)
        
    }
    
    @IBAction func btnPointsClick(_ sender: Any)
    {
        
        if  UserDefaults.standard.bool(forKey: "endtimer") == false
        {
            let date = Date()
            let formattt = DateFormatter()
            formattt.timeZone = NSTimeZone(name:"UTC")! as TimeZone
            formattt.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            
            let expirydate : Date = formattt.date(from: self.strExpiryDate)!
            
            let strCurrentDate = formattt.string(from: date)
            
            let currentDate1 : Date = (formattt.date(from: strCurrentDate)?.addingTimeInterval(10800))!
            
            
            let elapsed = expirydate.timeIntervalSince(currentDate1)
            
            self.Duration = Int(elapsed)
            
            let popOverVC = self.storyboard!.instantiateViewController(withIdentifier: "HomePopUp") as! HomePopUp
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            popOverVC.strBarcode = self.strBarcode
            popOverVC.Duration = self.Duration
            popOverVC.startTimer()
            self.present(popOverVC, animated: true)
        }
        else
        {
            wscallforGetBarcode()
        }
        
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotificationClick(_ sender: Any)
    {
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(detail, animated: true)

    }

    
    //MARK:- webservice
    
    func wscallforGetBarcode()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBarcode,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {

                        self.strBarcode = String(format: "%@", (result["Barcode"] as? String)!)
                                                
                        let date = Date()
                        self.strExpiryDate = result["Expiry_Time"] as! String
                        let formattt = DateFormatter()
                        formattt.timeZone = NSTimeZone(name:"UTC")! as TimeZone
                        formattt.dateFormat = "dd/MM/yyyy hh:mm:ss a"
                        
                        let expirydate : Date = formattt.date(from: self.strExpiryDate)!
                        
                        let strCurrentDate = formattt.string(from: date)
                        
                        let currentDate1 : Date = (formattt.date(from: strCurrentDate)?.addingTimeInterval(10800))!
                        
                        
                        let elapsed = expirydate.timeIntervalSince(currentDate1)
                        
                        self.Duration = Int(elapsed)
                        print(self.Duration)
                        
                        if self.Duration > 0
                        {
                            UserDefaults.standard.set(false, forKey: "endtimer")
                            UserDefaults.standard.synchronize()

                            self.btnPoints.isHidden = false
                            let popOverVC = self.storyboard!.instantiateViewController(withIdentifier: "HomePopUp") as! HomePopUp
                            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            popOverVC.strBarcode = self.strBarcode
                            popOverVC.Duration = self.Duration
                            popOverVC.startTimer()
                            self.present(popOverVC, animated: true)
                        }
                        
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforNews()
    {
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNews,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrayNews = NSMutableArray(array: (result["News"] as? Array)!)
                        self.pagerview1.reloadData()
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }

    func wscallforAdvertise()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetAdvertisements)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    self.wscallforNews()

                    if strStatus == "Success"
                    {
                        self.arrayAdv = NSMutableArray(array: (result["Advertisements"] as? Array)!)
                        
                        Manager.sharedInstance.Slidetime = result["Sliding_Time"] as! Int
                        
                        self.pageControl.numberOfPages = self.arrayAdv.count
                        self.collectionAdv.reloadData()
                        self.pagerview.reloadData()

                        if UserDefaults.standard.bool(forKey: "arabic")
                        {
                            self.collectionAdv.scrollToItem(at: IndexPath(item:0, section: 0), at: .centeredHorizontally, animated: false)
                        }
//
                    }
                    else
                    {
                    AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
            
            
        }
        
    }
    

}
