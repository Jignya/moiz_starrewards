//
//  HomePopUp.swift
//  Project
//
//  Created by HTNaresh on 18/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import CoreImage

class HomePopUp: UIViewController
{
    @IBOutlet weak var viewDetails: UIView!
    
    @IBOutlet weak var viewBarcodeNew: CDCodabarView!

    

    @IBOutlet weak var viewImgBg: ViewExtender!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lbldesc: UILabel!
    @IBOutlet weak var lblYourRewards: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblBarcodeId: UILabel!
    
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblYourLevel: UILabel!
    @IBOutlet weak var lbllevelStatus: UILabel!
    @IBOutlet weak var imgMember: UIImageView!


    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var viewBarcode: UIView!
    @IBOutlet weak var imgBarcode: UIImageView!
    @IBOutlet weak var btnDesc: UIButton!

    @IBOutlet weak var viewTap: UIView!
    @IBOutlet weak var timerRing: UICircularTimerRing!
    
    @IBOutlet weak var conUnlockHeight: NSLayoutConstraint!
    var countdownTimer: Timer!
    var member: NSNumber!

    
//    let timerRing = UICircularTimerRing()

    var strBarcode : String!
    var Duration : Int!


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        print(self.Duration)
        
//        lblCurrency.text = kCurrency
        lblWelcome.text = kwelcome.uppercased()
        lbldesc.text = kCompletePopupDesc
        lbldesc.halfTextColorChange(fullText: lbldesc.text!, changeText: kunlock.uppercased(), isUnderLine: true,choseClr: GlobalConstants.DarkTheme)
        lblYourRewards.text = kYourPoints.uppercased()
        lblYourLevel.text = kYourLevel.uppercased()

        
        viewDetails.setGradientBackground(view: viewDetails, colorTop: GlobalConstants.DarkTheme, colorBottom: GlobalConstants.lightTheme)
        
        viewImgBg.setGradientBackground(view: viewImgBg, colorTop: GlobalConstants.DarkTheme, colorBottom: GlobalConstants.lightTheme)
        
        
        

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.numberOfTapsRequired = 1
        self.viewTap.addGestureRecognizer(tap)
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if Manager.sharedInstance.worthPoint != nil
        {
            lblPrice.text = Manager.sharedInstance.earnPoint
            if Manager.sharedInstance.memberLevel  == 1
            {
                imgMember.image = UIImage(named: "blue.png")
                lbllevelStatus.text = "Blue"
            }
            else if Manager.sharedInstance.memberLevel  == 2
            {
                imgMember.image = UIImage(named: "silver.png")
                lbllevelStatus.text = "Silver"

            }
            else if Manager.sharedInstance.memberLevel  == 3
            {
                imgMember.image = UIImage(named: "gold.png")
                lbllevelStatus.text = "Gold"

            }
            else if Manager.sharedInstance.memberLevel == 4
            {
                imgMember.image = UIImage(named: "platinum.png")
                lbllevelStatus.text = "Platinum"

            }
            else if Manager.sharedInstance.memberLevel == 5
            {
                imgMember.image = UIImage(named: "vip.png")
                lbllevelStatus.text = "VIP"

            }
        }

       
        
        self.lblBarcodeId.text = strBarcode
        
//        viewBarcodeNew.code = strBarcode

        
          self.imgBarcode.image = Barcode.fromString(string: strBarcode)!
//        let templateImage = self.imgBarcode.image!.withRenderingMode(.alwaysTemplate)
//        self.imgBarcode.image = templateImage
//        self.imgBarcode.tintColor = UIColor.white


        
//        self.startTimer()
        

        
//        DispatchQueue.main.async {
//
//            self.timerRing.startTimer(to: Double(self.Duration)) { state in
//                switch state {
//                case .finished:
//                    print("finished")
//                case .continued(let time):
//                    print("continued: \(time)")
//                case .paused(let time):
//                    print("paused: \(time)")
//                }
//            }
//
//        }
//
        


    }
    
    //MARK:- Timer code
    
    func startTimer()
    {

        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        lblTimer.text = "\(timeFormatted(Duration))"
        
        if Duration != 0 {
            Duration -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer()
    {
        UserDefaults.standard.set(true, forKey: "endtimer")
        UserDefaults.standard.synchronize()

        countdownTimer.invalidate()
        lblTimer.isHidden = true
        self.dismiss(animated: true, completion: nil)

        
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    @objc func handleTap()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDescClick(_ sender: Any)
    {
    
        self.dismiss(animated: false)
        {
            let vc = UIApplication.topViewController() as! UIViewController
            if (vc is HomeVC)
            {
                let vc1 = vc as? HomeVC
                vc1?.strcomeFrom = "pop"
                vc1?.RedirectToProfile()
            }
        }

        
    }
    
    func getImage(image: UIImage, backgroundColor: UIColor)->UIImage?{
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        backgroundColor.setFill()
        //UIRectFill(CGRect(origin: .zero, size: image.size))
        let rect = CGRect(origin: .zero, size: image.size)
        let path = UIBezierPath(arcCenter: CGPoint(x:rect.midX, y:rect.midY), radius: rect.midX, startAngle: 0, endAngle: 6.28319, clockwise: true)
        path.fill()
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    

    
    func wscallforGetBarcode()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBarcode,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let strId = String(format: "%@", (result["Barcode"] as? String)!)
                        self.lblBarcodeId.text = strId
                        self.imgBarcode.image = Barcode.fromString(string: strId)
                        
                        let dateformat = DateFormatter()
                        dateformat.dateFormat = "dd/MM/yyyy HH:mm:ss"
                        let date = dateformat.date(from: (result["Expiry_Time"] as? String)!)
                        
                        let elapsed = date!.timeIntervalSince(Date())

                        let duration = Int(elapsed)
                        print(duration)
                        
                        if duration > 0
                        {
                            self.timerRing.startTimer(to: TimeInterval(duration)) { state in
                                switch state {
                                case .finished:
                                    print("finished")
                                case .continued(let time):
                                    print("continued: \(time)")
                                case .paused(let time):
                                    print("paused: \(time)")
                                }
                            }
                            
                        }
                        else
                        {
                            AJAlertController.initialization().showAlert(aStrMessage: "Your redemption time is expired now", aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                                if index == 1
                                {
                                }
                            }
                        }
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
            
            
        }
        
    }


}


public extension UIView {
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers! {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func fadeIn()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
        self.alpha = 1.0
        }, completion: nil)
    }
    
   func fadeOut()
   {
    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
        self.alpha = 0.7
        }, completion: nil)
    }
}

class Barcode {
    class func fromString(string : String) -> UIImage? {
        let data = string.data(using: .ascii)
        if let filter = CIFilter(name: "CICode128BarcodeGenerator")
        {
            filter.setValue(data, forKey: "inputMessage")
            if let outputCIImage = filter.outputImage {
            return UIImage(ciImage: outputCIImage)
            }
        }
        return nil  //
    }
}

