//
//  NotificationVC.swift
//  Project
//
//  Created by Jigar Joshi on 7/17/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class NotificationVC:  UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    
    var arrList : NSMutableArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblTitle.text = kNotification.uppercased()
        wscallforNotifications()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "0")
    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "TransactionCell"
        let cell:TransactionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! TransactionCell
        
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        cell.lblName.text = dict["TITLE"] as? String
        cell.lblDesc.text = dict["DESCRIPTION"] as? String
        cell.lblDay.text = dict["NOTIFICATION_DATE"] as? String
        cell.lbltime.text = ""
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let dict = arrList[indexPath.row] as! NSDictionary
        let nameString = dict["DESCRIPTION"] as! String
        let font : UIFont = UIFont.systemFont(ofSize: 15)
        
        let sizeOfText = nameString.height(withConstrainedWidth: (self.view.frame.size.width - 30), font: font)
        
        let cellSize = CGSize(width: (self.view.frame.size.width - 30), height:sizeOfText + 75)
        return cellSize
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func wscallforNotifications()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNotifications,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Notifications"] as? Array)!)
                        self.colList.reloadData()
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
}
