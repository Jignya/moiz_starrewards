//lbl
//  BrandCell.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class BrandCell: UICollectionViewCell
{
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    
    override func awakeFromNib()
    {
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

}
