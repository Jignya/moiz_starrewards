//
//  BrandVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class BrandVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var collectionList: UICollectionView!
    @IBOutlet weak var lblChooseBrand: UILabel!
    @IBOutlet weak var conColListHeight: NSLayoutConstraint!
    @IBOutlet weak var btnNext: ButtonExtender!

    var arrList : NSMutableArray = []
    
    var strBrandId : String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.wscallforBrands()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblAccount.text = kSuccessAcount
        lblChooseBrand.text = kChooseBrand
        btnNext.setTitle(kDone, for: .normal)


    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
//        Brand_Name
        
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Brand_Image_Url"] as! String)

        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let url = URL(string: urlString!)
        cell.imgLogo.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        cell.imgCheck.image = UIImage(named: "radio_uncheck")

        
        if dict["isSelect"] as? String == "1"
        {
            cell.imgCheck.image = UIImage(named: "radio_check")
        }
        
        conColListHeight.constant = collectionList.contentSize.height
        collectionList.updateConstraintsIfNeeded()
        collectionList.layoutIfNeeded()

        
        cell.layer.cornerRadius = 5.0
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.clipsToBounds = true

        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        let dic1 = dict.mutableCopy() as! NSMutableDictionary
        
        if dic1["isSelect"] as? String == "1"
        {
            dic1.setValue("0", forKey: "isSelect")
        }
        else
        {
            dic1.setValue("1", forKey: "isSelect")
        }
        
        self.arrList.replaceObject(at: indexPath.row, with: dic1)
        self.collectionList.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width:CGFloat = ((self.view.frame.size.width - 40) / 2) - 5
        let cellSize = CGSize(width:width , height:80)
        return cellSize
    }
    

    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClick(_ sender: Any)
    {
        let arr1 = Manager.sharedInstance.arrFilterDataforKey("1", withPredicateKey:"isSelect" , fromArray: self.arrList)
        
        if arr1.count == 0
        {
        AJAlertController.initialization().showAlert(aStrMessage: kBrandAlert, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                if index == 1
                {
                }
            }
        }
        else
        {
            strBrandId = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Brand_Id")
            wscallforFavBrands()
        }
        
       
    }
    
    //MARK:- webservice
    
    func wscallforBrands()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBrands,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Brands"] as? Array)!)
                        
                        for i in 0..<self.arrList.count
                        {
                            let dic = NSMutableDictionary(dictionary: self.arrList[i] as! [AnyHashable:Any])
                            let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                            dic1.setObject("0", forKey: "isSelect" as NSCopying)
                            self.arrList.replaceObject(at: i, with: dic1)
                        }
                        
                        self.collectionList.reloadData()
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforFavBrands()
    {
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wAddFavoriteBrands)
        
        let array = strBrandId.components(separatedBy: ", ")

        
        let dictPara:NSDictionary = ["BrandIds":array]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
}
