//
//  BrandDetailVC.swift
//  Project
//
//  Created by HTNaresh on 21/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class BrandDetailVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionAdv: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var contxtDescHeight: NSLayoutConstraint!
    @IBOutlet weak var conColAdvHeight: NSLayoutConstraint!

    @IBOutlet weak var btnlocation: UIButton!
    @IBOutlet weak var btnInformation: ButtonExtender!
    @IBOutlet weak var btnCollection: ButtonExtender!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnInsta: UIButton!
    
    var arrayAdv : NSMutableArray = []
    var arrayLocation = [[String:Any]]()
    var arrayCollection : NSMutableArray = []


    var timer = Timer()
    var BrndId : NSNumber!
    var indexRow : Int = 0
    
    var strFbLink : String!
    var strInstaLink : String!


    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        pageControl.numberOfPages = 1
        wscallforDetail()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        collectionAdv.isScrollEnabled = false
        
        btnCollection.setTitle(kCollection, for: .normal)
        btnInformation.setTitle(kInformation, for: .normal)

        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            pageControl.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        let leftGR = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeHandler_Left1(gestureRecognizer:)))
        leftGR.numberOfTouchesRequired = 1
        leftGR.direction = .left
        leftGR.delegate = self
        
        
        let RightGR = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeHandler_Right1(gestureRecognizer:)))
        RightGR.numberOfTouchesRequired = 1
        RightGR.direction = .right
        RightGR.delegate = self
        
        self.collectionAdv.addGestureRecognizer(leftGR)
        self.collectionAdv.addGestureRecognizer(RightGR)

    }
    
    @objc func swipeHandler_Left1(gestureRecognizer : UISwipeGestureRecognizer!) {
        if gestureRecognizer.state == .ended {
            // Perform action.
            
            swipeLeft1()
        }
    }
    
    @objc func swipeHandler_Right1(gestureRecognizer : UISwipeGestureRecognizer!) {
        if gestureRecognizer.state == .ended {
            // Perform action.
            
            swipeRight1()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        Manager.sharedInstance.addTabBar1(self, tab: "2")
        
    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return arrayAdv.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        let dict = arrayAdv[indexPath.row] as! NSDictionary
                
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Media_URL"] as! String)
        
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: urlString!)
        cell.imgLogo.setImageWith(url, usingActivityIndicatorStyle: .gray)
        cell.imgLogo.contentMode = .scaleAspectFit

        if indexPath.row == 0
        {
            DispatchQueue.main.async {

//                if cell.imgLogo.image != nil
//                {
//                    let aspectRatio = (cell.imgLogo.image as! UIImage).size.height/(cell.imgLogo.image as! UIImage).size.width
//                    let imageHeight = self.view.frame.width * aspectRatio
//                    self.setHeight(height: imageHeight)
//                }
//                else
//                {
                    self.downloadImage(from: url!)
//                }
            }

        }
        

        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: Double(Manager.sharedInstance.Slidetime), target: self, selector: #selector(timerAction), userInfo: nil, repeats:true)
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:self.view.frame.size.width , height:conColAdvHeight.constant)
        
        return cellSize
    }
    
    @objc func timerAction()
    {
        DispatchQueue.main.async
            {
                if self.indexRow + 1 < self.arrayAdv.count
                {
                    self.indexRow = self.indexRow + 1
                    
                    self.collectionAdv.scrollToItem(at: IndexPath(item:self.indexRow, section: 0), at: .centeredHorizontally, animated: false)
                }
                else
                {
                    self.indexRow = 0
                    self.collectionAdv.scrollToItem(at: IndexPath(item:0, section: 0), at: .centeredHorizontally, animated: false)
                    
                }
                
        }
        
    }
    
    @objc func swipeLeft1()
    {
        if collectionAdv.visibleCells.count > 0
        {
            var index: IndexPath? = collectionAdv.indexPathsForVisibleItems.last
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                if (index?.row ?? 0) < arrayAdv.count {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) - 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            else
            {
                if (index?.row ?? 0) + 1 < arrayAdv.count
                {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) + 1, section: 0), at: .centeredHorizontally, animated: false)
                }
            }
            
            
        }
    }
    
    
    @objc func swipeRight1()
    {
        if collectionAdv.visibleCells.count > 0 {
            var index: IndexPath? = collectionAdv.indexPathsForVisibleItems.first
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                if (index?.row ?? 0) + 1 < arrayAdv.count
                {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) + 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            else
            {
                if (index?.row ?? 0) < arrayAdv.count {
                    collectionAdv.scrollToItem(at: IndexPath(row: (index?.row ?? 0) - 1, section: 0), at: .centeredHorizontally, animated: false)
                }
                
            }
            
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        cell.alpha = 0.8
        UIView.animate(withDuration: 0.7)
        {
            cell.alpha = 1.0
        }
 
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = collectionAdv.frame.size.width
        pageControl.currentPage = Int(floor((collectionAdv.contentOffset.x / pageWidth)))
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    func downloadImage(from url: URL)
    {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async()
            {
                if data.isEmpty
                {
                    let img1 : UIImage = (UIImage(data: data) ?? nil)!
                    let aspectRatio = img1.size.height/img1.size.width
                    let imageHeight = self.view.frame.width * aspectRatio
                    self.setHeight(height: imageHeight)
                }
            }
        }
    }
    
    
    func setHeight(height:CGFloat)
    {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conColAdvHeight.constant = height
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
   
    @IBAction func btnCollectionClick(_ sender: Any)
    {
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "BrandCollectionVC") as! BrandCollectionVC
        detail.arrList = arrayCollection
        detail.strBrand = self.lblBrandName.text
        self.navigationController?.pushViewController(detail, animated: true)

    }
    
    @IBAction func btnInformationClick(_ sender: Any)
    {

    }
    
    @IBAction func btnFacebookClick(_ sender: Any)
    {
        if strFbLink != nil
        {
            if let url = URL(string: strFbLink) {
                UIApplication.shared.open(url)
            }
            
        }

    }
    
    @IBAction func btnInstaClick(_ sender: Any)
    {
//        var instagramHooks = "instagram://user?username=johndoe"
//        var instagramUrl = NSURL(string: instagramHooks)
//        if UIApplication.sharedApplication().canOpenURL(instagramUrl!) {
//            UIApplication.sharedApplication().openURL(instagramUrl!)
//        } else {
//            //redirect to safari because the user doesn't have Instagram
//            UIApplication.sharedApplication().openURL(NSURL(string: "http://instagram.com/")!)
//        }
        
        if strInstaLink != nil
        {
            if let url = URL(string: strInstaLink) {
                UIApplication.shared.open(url)
            }

        }

    }
    
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLocationClick(_ sender: Any)
    {
        if arrayLocation.count > 0
        {
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            detail.locations = arrayLocation
            detail.strComeFrom = "brands"
            detail.strTitle = lblTitle.text
            self.navigationController?.pushViewController(detail, animated: false)
        }
        else
        {
            AJAlertController.initialization().showAlert(aStrMessage: "Locations are not available.", aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                if index == 1
                {
                }
            }
        }
       
    }
    
    //MARK:- webservice
    
    func wscallforDetail()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBrandDetail,BrndId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrayAdv = NSMutableArray(array: (result["Media"] as? Array)!)
                        self.arrayCollection = NSMutableArray(array: (result["Collections"] as? Array)!)

                        self.arrayLocation = (result["Locations"] as? [[String:Any]])!
                        
                        self.collectionAdv.reloadData()
                        
                        self.pageControl.numberOfPages = self.arrayAdv.count

                        self.lblBrandName.text = result["Brand_Name"] as? String
                        self.lblTitle.text = result["Brand_Name"] as? String
                        
                        self.strFbLink = result["Facebook_URL"] as? String
                        self.strInstaLink = result["Instagram_URL"] as? String

                        
                        let getHeight:Float = result["Height"] as! Float
                        let getWidth:Float = result["Width"] as! Float
                        var  ratio:Float = 0
                        if getHeight != 0 || getWidth != 0
                        {
                            ratio = Float(getWidth/getHeight)
                            let newHeight = CGFloat(Float(self.collectionAdv.frame.size.width)/ratio)
                            self.conColAdvHeight.constant = CGFloat(newHeight)-1
                        }
                        
                        
                        self.txtDesc.text = (result["Brand_Desc"] as? String)!.htmlToString

                        let sizeThatFitsTextView1 = self.txtDesc.sizeThatFits(CGSize(width: self.txtDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
                        self.contxtDescHeight.constant = sizeThatFitsTextView1.height + 10
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    
}


