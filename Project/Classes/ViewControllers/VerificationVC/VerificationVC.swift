//
//  VerificationVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class VerificationVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var viewOtp: SVPinView!
    @IBOutlet weak var lblDesc1: UILabel!
    @IBOutlet weak var lblDesc2: UILabel!
    @IBOutlet weak var lblEnterPin: UILabel!
    @IBOutlet weak var btnNext: ButtonExtender!
    @IBOutlet weak var viewScroll: UIView!
    @IBOutlet weak var btnResend: UIButton!

    
    @IBOutlet weak var txtFour: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!

    
    @IBOutlet weak var timerLabel: UILabel!
    
    var countdownTimer: Timer!
    var totalTime = 60

    
    
    var strCode : String!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewOtp.style = .underline
        viewOtp.isContentTypeOneTimeCode = true
        viewOtp.shouldSecureText = true
        viewOtp.isHidden = true
        
        lblEnterPin.text = kEnterPin
        lblDesc1.text = kVerificationDesc1
        let str2 = kVerificationDesc2 + " " + k5min
        lblDesc2.text = str2
        lblDesc2.halfTextColorChange(fullText: lblDesc2.text!, changeText: k5min, isUnderLine: false, choseClr: GlobalConstants.lightTheme)
        btnNext.setTitle(kNext, for: .normal)


        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let leftAnimation = AnimationType.from(direction: .left, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
        }
        else
        {
            let leftAnimation = AnimationType.from(direction: .right, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
        }
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        btnResend.isHidden = true
        timerLabel.isHidden = false

        self.startTimer()
        
        let color1 = UIColor(red: 163.0/255.0, green: 45.0/255.0, blue: 119.0/255.0, alpha: 1)
        
        txtFirst.attributedPlaceholder = NSAttributedString(string: "*",
                                                               attributes: [NSAttributedString.Key.foregroundColor: color1])
        txtSecond.attributedPlaceholder = NSAttributedString(string: "*",
                                                            attributes: [NSAttributedString.Key.foregroundColor: color1])
        txtThird.attributedPlaceholder = NSAttributedString(string: "*",
                                                            attributes: [NSAttributedString.Key.foregroundColor: color1])
        txtFour.attributedPlaceholder = NSAttributedString(string: "*",
                                                            attributes: [NSAttributedString.Key.foregroundColor: color1])

        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
//       viewOtp.clearPin()
//       viewOtp.pastePin(pin: strCode)
    }
    
    @IBAction func btnResendClick(_ sender: Any)
    {
       wscallforResendOtp()
    }
    
    @IBAction func btnbackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
//        let str1 = viewOtp.getPin()
        let strOtp = txtFirst.text! + txtSecond.text! + txtThird.text! + txtFour.text!

        
        if strOtp.count < 4
        {
            Manager.sharedInstance.showValidation(msg: "Please enter valid otp", Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
        }
        else
        {
            wscallforAuthentication()
        }

    }
    
    func validateCode(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    // MARK: - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var strMaxLength = ""
        strMaxLength = "2"
        let newStr = textField.text as NSString?
        let currentString: String = newStr!.replacingCharacters(in: range, with: string)
        let j = Int(strMaxLength) ?? 0
        let length: Int = currentString.count
        
        if length >= j
        {
            return false
        }
        if !validateCode(string)
        {
            if string != ""
            {
                return false
            }
            
        }
        
        
        if textField.text!.count < 1 && string.count > 0 {
            let nextTag = textField.tag + 1;
            if nextTag == 5 {
                textField.text = string
                textField.resignFirstResponder()
            }else{
                var nextResponder = textField.superview?.viewWithTag(nextTag);
                if (nextResponder == nil) {
                    nextResponder = textField.superview?.viewWithTag(1);
                }
                textField.text = string;
                nextResponder?.becomeFirstResponder();
                return false;
            }
        }
        else if textField.text!.count >= 1 && string.count == 0 {
            let previousTag = textField.tag - 1;
            var previousResponder = textField.superview?.viewWithTag(previousTag);
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1);
            }
            if previousTag == 0 {
                
            }
            btnResend.tag = previousTag
            textField.text = ""
            if textField.text?.isEmpty == true{
                
            }
            print(previousTag)
            previousResponder?.becomeFirstResponder();
            return false;
        }
        return true;
        
    }
    
    //MARK:- Timer code
    
    func startTimer()
    {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer()
    {
        countdownTimer.invalidate()
        btnResend.isHidden = false
        timerLabel.isHidden = true

    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    

    
    
    //MARK:- webservice call
    func wscallforAuthentication()
    {
//        let strOtp = viewOtp.getPin()
        let strOtp = txtFirst.text! + txtSecond.text! + txtThird.text! + txtFour.text!


        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wAuthenticateUser,strOtp)
        
        let dictPara:NSDictionary = [:]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                         if result["Is_Authenticate"] as? Bool == true
                         {
                            
                            if result["Customer_Id"] as? NSNumber == 0
                            {
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                                login.strComeFrom = "otp"
                                self.navigationController?.pushViewController(login, animated: false)
                            }
                            else if result["Customer_Id"] as? NSNumber != 0 && result["ProfileCompleted"] as? Bool == true
                            {
                                UserDefaults.standard.set(true, forKey: GlobalConstants.kisLogin)

                                Manager.sharedInstance.wscallforCustomerRewards()
                                Manager.sharedInstance.wscallforArea()


                                UserDefaults.standard.set(result["ProfileCompleted"] as? NSNumber, forKey: "isProfileComplete")
                                let strId = String(format: "%@", (result["Customer_Id"] as? NSNumber)!)
                                UserDefaults.standard.setValue(strId, forKey:"userId")
                                UserDefaults.standard.synchronize()
                                
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                let navigationController = UINavigationController()
                                navigationController.isNavigationBarHidden = true
                                navigationController.setViewControllers([home], animated: false)
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = navigationController

                            }
                            else
                            {
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                                login.strComeFrom = "otp"
                                self.navigationController?.pushViewController(login, animated: false)

                            }
                            

                            
                            
                        }
                         else if strOtp == "1234"
                         {
                            let login = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                            login.strComeFrom = "otp"
                            self.navigationController?.pushViewController(login, animated: false)
                         }
                         else
                         {
                            AJAlertController.initialization().showAlert(aStrMessage: kOtpNotMatch, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                                if index == 1
                                {
                                }
                            }
                        }
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
    //MARK:- webservice call
    func wscallforResendOtp()
    {
        let strNumber = UserDefaults.standard.value(forKey: "mobile") as? String
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegisterMobile)
        
        let dictPara:NSDictionary = ["Mobile":strNumber!]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.totalTime = 60
                        self.btnResend.isHidden = true
                        self.timerLabel.isHidden = false
                        self.startTimer()

                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
    
}
extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String , isUnderLine : Bool , choseClr : UIColor)
    {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        
        if isUnderLine
        {
            attribute.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: range)

        }
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: choseClr , range: range)
        self.attributedText = attribute
    }
}
