//
//  CmsVC.swift
//  Project
//
//  Created by HTNaresh on 6/12/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class CmsVC: UIViewController
{
 
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var lblTitleWakala: UILabel!
    @IBOutlet weak var imgTitleLogo:UIImageView!

    
    var strTitle : String!
    var strRequest : String!


    override func viewDidLoad()
    {
        super.viewDidLoad()

        txtDesc.text = ""
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        lblTitle.text = strTitle.uppercased()
        
        if strTitle == kTier
        {
            strRequest = "TIER_BENEFITS"
        }
        else if strTitle == kAbout
        {
            strRequest = "ABOUT"
        }

        wscallforCMS()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        Manager.sharedInstance.addTabBar1(self, tab: "4")
        
    }
    

    //MARK:- button Method
    
    @IBAction func btnHomeClick(_ sender: Any)
    {
          self.navigationController?.popViewController(animated: true)
       
    }
    
    func displayHtmlDesc(htmlstr:String)
    {
        
        let htmlData = NSString(string: htmlstr).data(using: String.Encoding.unicode.rawValue)
        
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        
        let attritemutable =  NSMutableAttributedString(attributedString: attributedString)
        
        let paragraph = NSMutableParagraphStyle()
        
        paragraph.lineSpacing = 3
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            paragraph.alignment = .right
        }
        else
        {
            paragraph.alignment = .left
        }
        
        let attributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.paragraphStyle.rawValue): paragraph]
        
        attritemutable.addAttributes(attributes, range: NSMakeRange(0, attritemutable.length))
        
        let font = UIFont(name: GlobalConstants.kEnglishRegular , size: CGFloat(14.0))
        
        attritemutable.addAttributes([NSAttributedString.Key.font: font!], range: NSMakeRange(0, attritemutable.length))
        
//        attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
        
        self.txtDesc.attributedText = attritemutable
        
    }
    
    func wscallforCMS()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetCMSContent,strRequest,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")

        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    print(result)

                    let strStatus = result["Message"] as? String

                    if strStatus == "Success"
                    {
                        let str = (result["Content"] as? String)!
                        self .displayHtmlDesc(htmlstr: str)
                    }
                    else
                    {
                        self.txtDesc.text = ""
                    }
            }


        }

    }
}

extension NSMutableAttributedString {
    func setFontFace(font: UIFont, color: UIColor?) {
        beginEditing()
        self.enumerateAttribute(.font, in: NSRange(location: 0, length: self.length)) { (value, range, stop) in
            if let f = value as? UIFont, let newFontDescriptor = f.fontDescriptor.withFamily(font.familyName).withSymbolicTraits(f.fontDescriptor.symbolicTraits) {
                let newFont = UIFont(descriptor: newFontDescriptor, size: font.pointSize)
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(.foregroundColor, range: range)
                    addAttribute(.foregroundColor, value: color, range: range)
                }
            }
        }
        endEditing()
    }
}
