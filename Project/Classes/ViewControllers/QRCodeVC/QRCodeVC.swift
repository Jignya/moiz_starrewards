//
//  QRCodeVC.swift
//  Project
//
//  Created by Jigar Joshi on 7/19/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeVC: UIViewController,QRCodeReaderViewControllerDelegate
{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewScanner: UIView!
    
    

    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton        = false
            $0.showSwitchCameraButton = false
            $0.showCancelButton       = false
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0, y: 0, width: 1, height: 1)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        readerVC.delegate = self
        
        // Or by using the closure pattern
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            print(result)
            var userId = "0"
            if UserDefaults.standard.value(forKey: "userId") != nil
            {
                userId = UserDefaults.standard.value(forKey: "userId") as! String
            }
            
            var strValue = "0"
            
            if result?.value != nil
            {
                strValue = result!.value
            }
            
            
            let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wScanEvent,strValue,userId)
            
            let dictPara = NSDictionary()
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

                DispatchQueue.main.async
                    {
                        let strStatus = result["Message"] as? String
                        
                        if strStatus == "Success"
                        {
                            var strmsg = ""
                            if result["Points"] as? NSNumber != nil
                            {
                               strmsg = String(format: "You earned %@ points!", (result["Points"] as? NSNumber)!)
                            }
                            else
                            {
                                strmsg = "You earned points!"
                            }
                            
                            AJAlertController.initialization1().showAlert1(strTitle: "THANKS FOR ATTENDING EVENT", aStrMessage: strmsg, aCancelBtnTitle: "", aOtherBtnTitle: "", completion: { (index, title) in
                                
                                self.readerVC.startScanning()
                            })
                        }
                        else
                        {
                            AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                                if index == 1
                                {
                                }
                            }
                        }
                        
                }
                
                
            }
            
            
        }
        
       
        
        self.viewScanner.addSubview(readerVC.view)
        self.addChild(readerVC)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "4")
    }
    
    // MARK: - QRCodeReaderViewController Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    //This is an optional delegate method, that allows you to be notified when the user switches the cameraName
    //By pressing on the switch camera button
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
//        if let cameraName = newCaptureDevice.device.localizedName {
//            print("Switching capture to: \(cameraName)")
//        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
