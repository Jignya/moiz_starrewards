//
//  BrandCollectionVC.swift
//  Project
//
//  Created by Jigar Joshi on 7/15/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class BrandCollectionVC:  UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collectionBrand: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    
    var arrList : NSMutableArray = []
    var strBrand : String!

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        
        lblTitle.text = kCollection.uppercased()
        lblBrandName.text = strBrand.uppercased()
        
    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return  arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        if arrList.count > 0
        {
            let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
            
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Media_URL"] as! String)
            
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let url = URL(string: urlString!)
            cell.imgLogo.setImageWith(url, usingActivityIndicatorStyle: .gray)
            cell.imgLogo.contentMode = .scaleAspectFit
        }
        cell.layer.cornerRadius = 20.0
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        let Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Media_URL"] as! String)

        let detail = self.storyboard?.instantiateViewController(withIdentifier: "CollectionPreviewVC") as! CollectionPreviewVC
        detail.strBrand = self.lblBrandName.text
        detail.strImage = Stringimg
        self.navigationController?.pushViewController(detail, animated: false)

        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
        cell.contentView.animateAll(withType: [bottomAnimation])
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width:CGFloat = ((self.collectionBrand.frame.size.width) / 2) - 10
        let cellSize = CGSize(width:width , height:width)
        return cellSize
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
