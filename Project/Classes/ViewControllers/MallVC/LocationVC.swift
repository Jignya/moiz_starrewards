//
//  LocationVC.swift
//  Project
//
//  Created by HTNaresh on 22/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import MapKit

class LocationVC: UIViewController,MKMapViewDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnallBrnads: ButtonExtender!
    @IBOutlet weak var map: MKMapView!
    
    var dict : NSMutableDictionary = [:]
    var strComeFrom : String = ""
    var locations = [[String:Any]]()
    var strTitle : String!


    override func viewDidLoad()
    {
        super.viewDidLoad()

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblTitle.text = kStore
        
//        lblTitle.text = dict["Location_Name"] as? String
        
        if strComeFrom == "brands"
        {
            lblTitle.text = strTitle

            for location in locations
            {
                let strLat = location["Latitude"] as! String
                let strLong = location["Longitude"] as! String
                
                let annotation = MKPointAnnotation()
                annotation.title = location["Location_Name"] as? String
                annotation.coordinate = CLLocationCoordinate2D(latitude: Double(strLat)!, longitude: Double(strLong)!)
                map.addAnnotation(annotation)
            }
            
            map.showAnnotations(map.annotations, animated: true)

        }
        else
        {
            displayOnMap()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "2")
        if strComeFrom == "brands"
        {
            self.zoom(toFitMapAnnotations: map)
        }
    }
    
    func displayOnMap()
    {
        let strLat = dict["Latitude"] as! String
        let strLong = dict["Longitude"] as! String
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: Double(strLat)!, longitude: Double(strLong)!)
        map.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta:0.05 , longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        map.setRegion(region, animated: true)
    }
    
    
    func zoom(toFitMapAnnotations mapView: MKMapView?) {
        if mapView?.annotations.count == 0 {
            return
        }
        
        var topLeftCoord = CLLocationCoordinate2D()
        topLeftCoord.latitude = CLLocationDegrees(-90)
        topLeftCoord.longitude = CLLocationDegrees(180)
        
        var bottomRightCoord = CLLocationCoordinate2D()
        bottomRightCoord.latitude = CLLocationDegrees(90)
        bottomRightCoord.longitude = CLLocationDegrees(-180)
        
        for annotation in mapView?.annotations as? [MKPointAnnotation] ?? [] {
            topLeftCoord.longitude = CLLocationDegrees(fmin(Float(topLeftCoord.longitude), Float(annotation.coordinate.longitude)))
            topLeftCoord.latitude = CLLocationDegrees(fmax(Float(topLeftCoord.latitude), Float(annotation.coordinate.latitude)))
            
            bottomRightCoord.longitude = CLLocationDegrees(fmax(Float(bottomRightCoord.longitude), Float(annotation.coordinate.longitude)))
            bottomRightCoord.latitude = CLLocationDegrees(fmin(Float(bottomRightCoord.latitude), Float(annotation.coordinate.latitude)))
        }
        
        var region: MKCoordinateRegion?
        region?.center.latitude = CLLocationDegrees(topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5)
        region?.center.longitude = CLLocationDegrees(topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5)
        region?.span.latitudeDelta = CLLocationDegrees(fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1) // Add a little extra space on the sides
        region?.span.longitudeDelta = CLLocationDegrees(fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1) // Add a little extra space on the sides
        
        if var region = region {
            region = (mapView?.regionThatFits(region))!
        }
        if let region = region {
            map.setRegion(region, animated: true)
        }
        
    }

    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView = av
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "STORES.png")
        }
        
        return annotationView
    }

    @IBAction func btnbackClicl(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAllBrandsClick(_ sender: Any) {
    }
    

}
