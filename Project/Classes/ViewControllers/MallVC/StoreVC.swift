//
//  StoreVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class StoreVC: UIViewController,CarbonTabSwipeNavigationDelegate
{
    @IBOutlet weak var CarbonView: UIView!
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()


    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.style()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        Manager.sharedInstance.addTabBar1(self, tab: "2")

    }
    
    func style()
    {
        
        let items = [kBrand.uppercased(), kMall.uppercased()]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        let color: UIColor = GlobalConstants.lightTheme
        carbonTabSwipeNavigation.toolbar.isTranslucent = true
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        carbonTabSwipeNavigation.carbonSegmentedControl!.backgroundColor = UIColor.black.withAlphaComponent(0.10)
        carbonTabSwipeNavigation.carbonSegmentedControl!.isOpaque = false
        
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setTabExtraWidth(0)
        carbonTabSwipeNavigation.setIndicatorHeight(5)
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/2, forSegmentAt: 1)
        carbonTabSwipeNavigation.setNormalColor(GlobalConstants.DarkTheme, font: UIFont(name: GlobalConstants.kEnglishRegular, size: 16)!)
        carbonTabSwipeNavigation.setSelectedColor(GlobalConstants.lightTheme, font: UIFont(name: GlobalConstants.kEnglishRegular, size: 16)!)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: CarbonView)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        switch index
        {
        case 0:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "Brands1VC") as! Brands1VC
            return obj
        case 1:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MallVC") as! MallVC
            return obj
       
        default:
            return self.storyboard?.instantiateViewController(withIdentifier: "StoreVC") as! StoreVC
        }
        
    }
    
}
