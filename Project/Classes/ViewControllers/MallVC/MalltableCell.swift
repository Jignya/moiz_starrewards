//
//  MalltableCell.swift
//  Project
//
//  Created by HTNaresh on 18/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class MalltableCell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArw: UIImageView!


    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
