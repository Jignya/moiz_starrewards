//
//  MallVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class MallVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var collectionCountry: UICollectionView!
    @IBOutlet weak var tblList: UITableView!
    
    var arrArea : NSMutableArray = []
    var arrLocation : NSMutableArray = []

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        if UserDefaults.standard.bool(forKey: "arabic")
//        {
//            collectionCountry.transform = CGAffineTransform(scaleX: -1, y: 1)
//
//        }


        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        arrArea = Manager.sharedInstance.arrArea.mutableCopy() as! NSMutableArray
        
        for i in 0..<arrArea.count
        {
            let dic = NSMutableDictionary(dictionary: arrArea[i] as! [AnyHashable:Any])
            let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
            if i == 0
            {
                dic1.setObject("1", forKey: "isSelect" as NSCopying)
            }
            else
            {
                dic1.setObject("0", forKey: "isSelect" as NSCopying)
            }
            arrArea.replaceObject(at: i, with: dic1)
        }
        
        
        let dict = arrArea[0] as! [String:Any]
        wscallforLocations(areaId: (dict["Area_Id"] as? NSNumber)!)
        
        
//        collectionCountry.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrArea.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }

        
        
        let dict = NSMutableDictionary(dictionary: arrArea[indexPath.row] as! Dictionary)
        
        cell.lblTitle.textColor = UIColor.lightText
        
        cell.lblTitle.text = dict["Area_Name"] as? String
        
        if dict["isSelect"] as? String == "1"
        {
            cell.lblTitle.textColor = UIColor.white
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = arrArea[indexPath.row] as! [String:Any]
        
        for i in 0..<arrArea.count
        {
            let dic = NSMutableDictionary(dictionary: arrArea[i] as! [AnyHashable:Any])
            let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
            if i == indexPath.row
            {
                dic1.setObject("1", forKey: "isSelect" as NSCopying)
                wscallforLocations(areaId: (dict["Area_Id"] as? NSNumber)!)

            }
            else
            {
                dic1.setObject("0", forKey: "isSelect" as NSCopying)
            }
            arrArea.replaceObject(at: i, with: dic1)
        }
        
        collectionCountry.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let dict = arrArea[indexPath.row] as! NSDictionary
        let nameString = dict["Area_Name"] as! String
        let font : UIFont = UIFont.systemFont(ofSize: 15)
        let attributes = NSDictionary(object: font, forKey:NSAttributedString.Key.font as NSCopying)
        
        let sizeOfText = nameString.size(withAttributes: (attributes as! [NSAttributedString.Key : Any]))
        
        let cellSize = CGSize(width:(sizeOfText.width + 30) , height:collectionCountry.frame.size.height)
        return cellSize
    }
    
    //MARK:- tableview Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "MalltableCell"
        let cell:MalltableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MalltableCell
        
        let dict = NSMutableDictionary(dictionary: arrLocation[indexPath.row] as! Dictionary)
        
        cell.lblTitle.text = dict["Location_Name"] as? String

        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrLocation[indexPath.row] as! Dictionary)
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        detail.dict = dict.mutableCopy() as! NSMutableDictionary
        self.navigationController?.pushViewController(detail, animated: false)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 45
    }
    
    //MARK:- webservice
    
    func wscallforLocations(areaId : NSNumber)
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetLocations ,areaId ,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrLocation = NSMutableArray(array: (result["Locations"] as? Array)!)
                        self.tblList.reloadData()
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
}
