//
//  Brands1VC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class Brands1VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    
    @IBOutlet weak var collectionBrand: UICollectionView!
    
    var arrList : NSMutableArray = []
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        viewSearch.layer.cornerRadius = viewSearch.frame.size.height / 2
        viewSearch.clipsToBounds = true
        
        wscallforBrands()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        txtSearch.placeholder = kSearch


    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        //        Brand_Name
        
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Brand_Image_Url"] as! String)
        
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: urlString!)
        cell.imgLogo.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        
        cell.layer.cornerRadius = 5.0
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.clipsToBounds = true

        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)

        let login = self.storyboard?.instantiateViewController(withIdentifier: "BrandDetailVC") as! BrandDetailVC
        login.BrndId = dict["Brand_Id"] as? NSNumber
        self.navigationController?.pushViewController(login, animated: false)

        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
        cell.contentView.animateAll(withType: [bottomAnimation])
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width:CGFloat = ((self.view.frame.size.width - 40) / 2) - 5
        let cellSize = CGSize(width:width , height:80)
        return cellSize
    }
    
    
    //MARK:- webservice
    
    func wscallforBrands()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBrands,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Brands"] as? Array)!)
                        self.collectionBrand.reloadData()
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }

}
