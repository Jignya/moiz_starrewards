//
//  ProfileVC.swift
//  Project
//
//  Created by HTNaresh on 20/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var viewScroll: UIView!
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblYouAcnt: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnDone: ButtonExtender!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet var btnMale:UIButton!
    @IBOutlet var btnFemale:UIButton!
    @IBOutlet var btnTerms:UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet var btnAccept:UIButton!
    @IBOutlet weak var lblTerms1: UILabel!
    @IBOutlet weak var viewtap: UIView!
    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var conViewTermsheight: NSLayoutConstraint!
    @IBOutlet weak var lblBirthday: UILabel!
    


    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtmonth: UITextField!
    @IBOutlet weak var txtYear: UITextField!

    var strdateFormat : String!

    let datePicker1 = UIDatePicker()
    var strComeFrom : String!
    var strDateReuest : String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        conViewTermsheight.constant = 0
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let leftAnimation = AnimationType.from(direction: .left, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
        }
        else
        {
            let leftAnimation = AnimationType.from(direction: .right, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
        }
        
        
        lblCreate.text = kCreate
        lblYouAcnt.text = kYourAccount
        lblGender.text = kGender
        lblBirthday.text = kBirthday
        lblMale.text = kMale
        lblFemale.text = kFemale
        btnDone.setTitle(kDone, for: .normal)
        btnAccept.setTitle(kAssent, for: .normal)

        let str1 = kTermsDesc + " " + kTerms.uppercased()
        lblTerms1.text = str1
        lblTerms.halfTextColorChange(fullText: lblTerms1.text!, changeText: kTerms.uppercased() , isUnderLine: true,choseClr: GlobalConstants.lightTheme)
        
        lblDesc.text = kTermsCondition
        
        
        
        txtFirstName.attributedPlaceholder = NSAttributedString(string: kFirstname,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
        
        txtLastName.attributedPlaceholder = NSAttributedString(string: kLastname,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])

        txtBirthday.attributedPlaceholder = NSAttributedString(string: kBirthday,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
        
        txtYear.attributedPlaceholder = NSAttributedString(string: "YYYY",attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
        
        txtmonth.attributedPlaceholder = NSAttributedString(string: "MMMM",attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])

        txtDate.attributedPlaceholder = NSAttributedString(string: "dd",attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])


        
        if strComeFrom == "otp"
        {
            btnBack.isHidden = true
            imgBack.isHidden = true
            lblCreate.text = kCreate
            lblTerms.isHidden = false
            btnTerms.isHidden = false

            
        }
        else
        {
            btnBack.isHidden = false
            imgBack.isHidden = false
            lblCreate.text = "Complete"
            lblTerms.isHidden = true
            btnTerms.isHidden = true
            wscallforGetProfile()


        }
        
        btnAccept.isUserInteractionEnabled = true
        btnAccept.isSelected = false
        
        let image = UIImage(named: "Terms_check.png")?.withRenderingMode(.alwaysTemplate)
        btnAccept.setImage(image, for: .selected)
        btnAccept.tintColor = GlobalConstants.DarkTheme
        
        let image1 = UIImage(named: "Terms_uncheck.png")?.withRenderingMode(.alwaysTemplate)
        btnAccept.setImage(image1, for: .normal)
        btnAccept.tintColor = GlobalConstants.DarkTheme

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap1))
        tap.numberOfTapsRequired = 1
        self.viewtap.addGestureRecognizer(tap)
        
        


    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        if strComeFrom != "otp"
        {
           Manager.sharedInstance.addTabBar1(self, tab: "1")
        }
    }
    
    @objc func handleTap1()
    {
        
        if btnAccept.isSelected
        {
            btnTerms.isSelected = true
        }
        else
        {
            btnTerms.isSelected = false
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.conViewTermsheight.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    //MARK:- Textfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtYear
        {
            strdateFormat = "yyyy"
            SetdatePicker()
        }
        else if textField == txtmonth
        {
            strdateFormat = "MMMM"
            SetdatePicker()
        }
        else if textField == txtDate
        {
            strdateFormat = "dd"
            SetdatePicker()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtYear || textField == txtmonth || textField == txtDate
        {
            return false
        }
        
        return true
    }
    
    //MARK:- Set datepicker
    func SetdatePicker()
    {
        datePicker1.datePickerMode = .date
        datePicker1.addTarget(self, action: #selector(self.updateTextField(_:)), for: .valueChanged)
        let formattt = DateFormatter()
        formattt.dateFormat = strdateFormat
        datePicker1.maximumDate = Date()
        if strdateFormat == "yyyy"
        {
            txtYear.inputView = datePicker1
        }
        else if strdateFormat == "MMMM"
        {
            txtmonth.inputView = datePicker1
        }
        else if strdateFormat == "dd"
        {
            txtDate.inputView = datePicker1
        }
        
    }
    
    @objc  func updateTextField(_ sender:UIDatePicker)
    {
        let dateForm1 = DateFormatter()
        dateForm1.dateFormat = "dd/MMMM/yyyy"
        let date1 = sender.date
        let strDate = dateForm1.string(from: date1)
        
        let dateForm2 = DateFormatter()
        dateForm2.dateFormat = "dd/MMM/yyyy"
        strDateReuest = dateForm1.string(from: sender.date)

        
        let ar1 = strDate.components(separatedBy: "/")

        txtYear.text = ar1[2]
        txtmonth.text = ar1[1]
        txtDate.text = ar1[0]

    }
    
    @IBAction func btnTermsClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected

    }
    
    @IBAction func lblTermsClick(_ sender: Any)
    {
        self.conViewTermsheight.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conViewTermsheight.constant = self.view.frame.size.height
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }

    
    @IBAction func btnMaleClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnMale.isSelected = true
        btnFemale.isSelected = false
    }
    
    @IBAction func btnFemaleClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnMale.isSelected = false
        btnFemale.isSelected = true

    }
    
    @IBAction func btnDoneClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtFirstName.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kalertFirstName, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else if txtLastName.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kAlertLastname, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else if txtYear.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kAlertDob, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

        }
        else if  !btnMale.isSelected  &&  !btnFemale.isSelected
        {
            Manager.sharedInstance.showValidation(msg: kAlertGender, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else if  !btnTerms.isSelected
        {
            Manager.sharedInstance.showValidation(msg: kAlertterms, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else
        {
            wscallforUpdateProfile()
        }
       
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAcceptClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
    }

    
    //MARK:- webservice call
    func wscallforUpdateProfile()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        var strGender = ""
        if btnMale.isSelected
        {
            strGender = "Male"
        }
        else
        {
            strGender = "Female"
        }
        
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateProfile)
        
        let dictPara:NSDictionary = ["CustomerId":userId ,"FirstName":self.txtFirstName.text!,"LastName":self.txtLastName.text!,"Gender":strGender,"DOB":strDateReuest,"CountryCode":(UserDefaults.standard.value(forKey: "code") as? String)!,"Mobile":(UserDefaults.standard.value(forKey: "mobile") as? String)!,"Email":"","Nationality_Id":0,"ProfileCompleted":0]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {

                        let strId = String(format: "%@", (result["CustomerId"] as? NSNumber)!)
                        UserDefaults.standard.setValue(strId, forKey:"userId")
                        
                        let name = self.txtFirstName.text! + " " + self.txtLastName.text!
                        
                        UserDefaults.standard.set(name, forKey: "name")
                        

                        UserDefaults.standard.synchronize()
                        
                        
                        
                        
                        if userId == "0"
                        {
                            UserDefaults.standard.set(true, forKey: GlobalConstants.kisLogin)
                            UserDefaults.standard.synchronize()
                        Manager.sharedInstance.wscallforCustomerRewards()
                            
                        }
                        
                        let login = self.storyboard?.instantiateViewController(withIdentifier: "BrandVC") as! BrandVC
                        self.navigationController?.pushViewController(login, animated: false)

                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
    
    func wscallforGetProfile()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserProfile,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.txtFirstName.text = (result["FirstName"] as! String)
                        
                        self.txtLastName.text = (result["LastName"] as! String)
                        
                        let strDate = result["DOB"] as! String

                        let formattt = DateFormatter()
                        formattt.dateFormat = "dd/MM/yyyy"

                        let date = formattt.date(from: strDate)
                        
                        formattt.dateFormat = "dd/MMMM/yyyy"

                        let strDate1 = formattt.string(from: date!)
                        
                        let ar1 = strDate1.components(separatedBy: "/")
                        
                        self.txtYear.text = ar1[2]
                        self.txtmonth.text = ar1[1]
                        self.txtDate.text = ar1[0]
                        
                        let strGender = result["Gender"] as! String
                        
                        if strGender == "Male"
                        {
                            self.btnMale.isSelected = true
                            self.btnFemale.isSelected = false
                        }
                        else
                        {
                            self.btnMale.isSelected = false
                            self.btnFemale.isSelected = true
                        }


                        
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
            
            
        }
        
    }
    
}
