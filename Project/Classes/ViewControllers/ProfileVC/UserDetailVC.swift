//
//  UserDetailVC.swift
//  Project
//
//  Created by HTNaresh on 30/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import AVKit

class UserDetailVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCompleteted: UILabel!

    @IBOutlet weak var viewScroll: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var contxtAddHeight: NSLayoutConstraint!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtmonth: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtLanguage: UITextField!
    
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!

    @IBOutlet weak var lblProfileStatus: UILabel!
    @IBOutlet weak var conProfileImageCenter: NSLayoutConstraint!
    
    @IBOutlet weak var viewStatus: UIView!
    var strDateReuest : String!



    var strdateFormat : String!

    
    @IBOutlet weak var btnUpdate: ButtonExtender!
    let datePicker1 = UIDatePicker()
    var CountryId : NSNumber!
    var arrnationalities : NSMutableArray = []
    var strLang : String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtMobile.isUserInteractionEnabled = false
        
//        txtMobile.text = UserDefaults.standard.value(forKey: "mobile") as? String
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let leftAnimation = AnimationType.from(direction: .left, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
            txtAddress.textAlignment = .right
            
        }
        else
        {
            let leftAnimation = AnimationType.from(direction: .right, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])
            
            txtAddress.textAlignment = .left

            
        }
        
        lblTitle.text = kprofile.uppercased()
        lblGender.text = kGender
        txtBirthday.placeholder = kBirthday
        lblMale.text = kMale
        lblFemale.text = kFemale
        btnUpdate.setTitle(kupdate.uppercased(), for: .normal)
        

        wscallforGetProfile()
        setTint()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "4")
        
//        if UserDefaults.standard.value(forKey: "isProfileComplete") as? NSNumber == 1
//        {
//            conProfileImageCenter.constant = 0
//            viewStatus.isHidden = true
//        }
    }
    
    func setTint()
    {
        txtCountry.setLeftPaddingPoints(8)
        txtCountry.setRightPaddingPoints(8)
        txtLanguage.setLeftPaddingPoints(8)
        txtLanguage.setRightPaddingPoints(8)

        lblFirstName.text = kFirstname
        lblLastName.text = kLastname
        lblEmail.text = kEmail
        lblMobile.text = kMobNum
        lblCountry.text = kNationality
        lblBirthday.text = kBirthday
        lblLanguage.text = klang
        
//        txtFirstName.attributedPlaceholder = NSAttributedString(string: kFirstname,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtLastName.attributedPlaceholder = NSAttributedString(string: kLastname,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtBirthday.attributedPlaceholder = NSAttributedString(string: kBirthday,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtEmail.attributedPlaceholder = NSAttributedString(string: kEmail,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtMobile.attributedPlaceholder = NSAttributedString(string: kMobile,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtCountry.attributedPlaceholder = NSAttributedString(string: kCountry,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
//        txtCity.attributedPlaceholder = NSAttributedString(string: kCity,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])
//
        
//        txtAddress.attributedPlaceholder = NSAttributedString(string: kHomeAddress,attributes: [NSAttributedString.Key.foregroundColor: GlobalConstants.DarkTheme])


        
    }
    
  
    
    //MARK:- Set datepicker
    func SetdatePicker()
    {
        datePicker1.datePickerMode = .date
        datePicker1.addTarget(self, action: #selector(self.updateTextField(_:)), for: .valueChanged)
        let formattt = DateFormatter()
        formattt.dateFormat = strdateFormat
        datePicker1.maximumDate = Date()
        if strdateFormat == "yyyy"
        {
            txtYear.inputView = datePicker1
        }
        else if strdateFormat == "MMMM"
        {
            txtmonth.inputView = datePicker1
        }
        else if strdateFormat == "dd"
        {
            txtDate.inputView = datePicker1
        }
        
    }
    
    @objc  func updateTextField(_ sender:UIDatePicker)
    {
        let dateForm1 = DateFormatter()
        dateForm1.dateFormat = "dd/MMMM/yyyy"
        let date1 = sender.date
        let strDate = dateForm1.string(from: date1)
        
        let dateForm2 = DateFormatter()
        dateForm2.dateFormat = "dd/MMM/yyyy"
        strDateReuest = dateForm1.string(from: sender.date)

        
        let ar1 = strDate.components(separatedBy: "/")
        
        txtYear.text = ar1[2]
        txtmonth.text = ar1[1]
        txtDate.text = ar1[0]
        
    }
    

    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMaleClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnMale.isSelected = true
        btnFemale.isSelected = false
    }
    
    @IBAction func btnFemaleClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnMale.isSelected = false
        btnFemale.isSelected = true
        
    }
    
    @IBAction func btnUserClick(_ sender: Any)
    {
        self.view.endEditing(true)
        showActionSheet()
    }
    
    
    func showActionSheet()
    {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction(title: kCamera, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.Camera()
        }))
        actionSheet.addAction(UIAlertAction(title: kGallary, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.openGallary()
        }))
        actionSheet.addAction(UIAlertAction(title: kCancel, style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //PickerView Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgUser.contentMode = .scaleToFill
        imgUser.image = chosenImage
        dismiss(animated:true, completion: nil)
        
    }
    
    
    
    func Camera()
    {
        self.view.endEditing(true)
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            
            if status == .authorized {
                // authorized
                picker.sourceType = .camera
                picker.delegate = self
                picker.allowsEditing = false
                present(picker, animated: true)
            } else if status == .denied {
                // denied
                let alertController = UIAlertController(title:"Warning", message:"App does not have permission to access camera.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    return
                }
                
                
                // Add the actions
                alertController.addAction(okAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
                
            } else if status == .restricted {
                // restricted
            } else if status == .notDetermined {
                // not determined
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                    if granted {
                        print("Granted access")
                    } else {
                        print("Not granted access")
                    }
                })
            }
        } else {
            let alertController = UIAlertController(title:"Warning", message:"Camera is not avaliable.", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                return
            }
            
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        
    }
    
    @IBAction func btnUpdateClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtFirstName.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kalertFirstName, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else if txtLastName.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kAlertLastname, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
        else if  !btnMale.isSelected  &&  !btnFemale.isSelected
        {
            Manager.sharedInstance.showValidation(msg: kAlertGender, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            
        }
            
        else if txtYear.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kAlertDob, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
        }
            
        else if (txtEmail.text == "")
        {
            Manager.sharedInstance.showValidation(msg: kPleaseenteryouremailaddressEn, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

        }
            
        else if(!(isValidEmail(testStr: txtEmail.text!)))
        {
            Manager.sharedInstance.showValidation(msg: kEmailValidationEn, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

        }
        else if(txtMobile.text=="")
        {
            Manager.sharedInstance.showValidation(msg: kPleaseenteryourmobileEn, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

        }
        else if((txtMobile.text?.count)! < 8)
        {
            Manager.sharedInstance.showValidation(msg: kPleaseentervalidMobnum, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

        }
        else if txtCountry.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kSelCountry, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
        }
//        else if txtCity.text == ""
//        {
//            Manager.sharedInstance.showValidation(msg: kSelCity, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
//        }
//        else if txtLanguage.text == ""
//        {
//            Manager.sharedInstance.showValidation(msg: keSelLang, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
//        }
        else
        {
           wscallforupdateProfile()
        }
        
    }
    
    @IBAction func btnLanguageClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let arrLang = [["name":"English"],["name":"Arabic"]]
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            if dictSelect["name"] as? String == "English"
            {
                self.strLang = "E"
            }
            else
            {
                self.strLang = "A"
            }
            
            self.txtLanguage.text = dictSelect["name"] as? String
            
        }, arrData: NSMutableArray(array: arrLang as! Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: false, isother: true,self)

    }
    
    @IBAction func btnCountryClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
//        let arrCountry = Manager.sharedInstance.arrcountry.mutableCopy()
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            self.CountryId = (dictSelect["Nationality_Id"] as! NSNumber)
            self.txtCountry.text = dictSelect["Nationality_Name"] as? String
            
            
        }, arrData: arrnationalities, isLanguage: false, isNationality: false, isCategories: true, isProfession: false, isother: false,self)
    }
    
    @IBAction func btnCityClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtCountry.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kSelCountry, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
            return
        }
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetAreas,CountryId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let arrCountry = NSMutableArray(array: result["Areas"] as! Array)
                    
                    Manager.sharedInstance.addCustomPopup({ (dictSelect) in
                        
                        //            self.countryId = (dictSelect["Country_Id"] as! NSNumber)
                        self.txtCity.text = dictSelect["Area_Name"] as? String
                        
                        
                    }, arrData: NSMutableArray(array: arrCountry as! Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: true, isother: false,self)
            }
        }
        
        
    }

    
    
    
   
    
    // MARK: -Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtYear || textField == txtmonth || textField == txtDate || textField == txtBirthday || textField == txtCountry || textField == txtCity
        {
            return false
        }
        
        else if textField == txtMobile
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "8"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtYear
        {
            strdateFormat = "yyyy"
            SetdatePicker()
        }
        else if textField == txtmonth
        {
            strdateFormat = "MMMM"
            SetdatePicker()
        }
        else if textField == txtDate
        {
            strdateFormat = "dd"
            SetdatePicker()
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        let sizeThatFitsTextView1 = self.txtAddress.sizeThatFits(CGSize(width: self.txtAddress.frame.size.width, height: CGFloat(MAXFLOAT)))
        self.contxtAddHeight.constant = sizeThatFitsTextView1.height
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        return true
    }
    
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9,+]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    
    
    // Password length validation
    func isPwdLenth(_ password: String) -> Bool {
        if (!(password.characters.count < 5 || password.characters.count > 20)){
            return true
        }
        else{
            return false
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func wscallforNationalities()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNationalities,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    self.arrnationalities = NSMutableArray(array: result["Nationalities"] as! Array)
                    
                    let arr1 = Manager.sharedInstance.arrFilterDataforKey1(String(format: "%@", self.CountryId), withPredicateKey: "Nationality_Id", fromArray: self.arrnationalities)
                    
                    if arr1.count > 0
                    {
                        let dict = arr1[0] as! [String:Any]
                        self.txtCountry.text = dict["Nationality_Name"] as? String
                        
                    }
            }
            
            
        }
        
    }
    
    func wscallforupdateProfile()
    {
        
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        var strGender = ""
        if btnMale.isSelected
        {
            strGender = "Male"
        }
        else
        {
            strGender = "Female"
        }
        
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateProfile)
        
        let dictPara:NSDictionary = ["CustomerId":userId ,"FirstName":self.txtFirstName.text!,"LastName":self.txtLastName.text!,"Gender":strGender,"DOB":strDateReuest,"CountryCode":(UserDefaults.standard.value(forKey: "code") as? String)!,"Mobile":(UserDefaults.standard.value(forKey: "mobile") as? String)!,"Email":self.txtEmail.text!,"Nationality_Id":self.CountryId,"ProfileCompleted":1]

        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.wscallforGetProfile()
                        let name = self.txtFirstName.text! + " " + self.txtLastName.text!
                        
                        UserDefaults.standard.set(name, forKey: "name")

                        UserDefaults.standard.set(self.txtMobile.text!, forKey: "mobile")

                        UserDefaults.standard.synchronize()

                       self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
    
    func wscallforGetProfile()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userId") != nil
        {
            userId = UserDefaults.standard.value(forKey: "userId") as! String
        }
        
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserProfile,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.txtFirstName.text = (result["FirstName"] as! String)
                        
                        self.txtLastName.text = (result["LastName"] as! String)
                        
                        self.txtEmail.text = result["Email"] as! String
                        
                        self.txtMobile.text = String(format: "+ %@ %@", result["CountryCode"] as! String, result["Mobile"] as! String)

                        
                        let strDate = result["DOB"] as! String
                        
                        
                        let formattt = DateFormatter()
                        formattt.dateFormat = "dd/MMM/yyyy"
                        
                        let date = formattt.date(from: strDate)
                        
                        self.strDateReuest = formattt.string(from: date!)

                        
                        formattt.dateFormat = "dd/MMMM/yyyy"
                        
                        let strDate1 = formattt.string(from: date!)
                        

                        
                        let ar1 = strDate1.components(separatedBy: "/")
                        
                        self.txtYear.text = ar1[2]
                        self.txtmonth.text = ar1[1]
                        self.txtDate.text = ar1[0]
                        
                        let strGender = result["Gender"] as! String
                        
                        if strGender == "Male"
                        {
                            self.btnMale.isSelected = true
                            self.btnFemale.isSelected = false
                        }
                        else
                        {
                            self.btnMale.isSelected = false
                            self.btnFemale.isSelected = true
                        }
                        
                        if result["Preferred_Language"] as? String != nil
                        {
                            self.txtLanguage.text = result["Preferred_Language"] as! String == "E" ? "English" : "Arabic"

                        }
                        
                        UserDefaults.standard.set(result["ProfileCompleted"] as? NSNumber, forKey: "isProfileComplete")
                        UserDefaults.standard.synchronize()
                        
                        if UserDefaults.standard.value(forKey: "isProfileComplete") as? NSNumber == 1
                        {
                            self.lblProfileStatus.text = "100%"

                        }
                        else
                        {
                            self.lblProfileStatus.text = "63%"
                        }



                        self.CountryId = result["Nationality_Id"] as! NSNumber
                        
                        self.wscallforNationalities()
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
            
            
        }
        
    }
    
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    var isNumeric: Bool {
        guard self.characters.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self.characters).isSubset(of: nums)
    }
}
