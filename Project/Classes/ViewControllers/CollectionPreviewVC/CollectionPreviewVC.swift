//
//  CollectionPreviewVC.swift
//  Project
//
//  Created by Jigar Joshi on 7/17/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CollectionPreviewVC: UIViewController
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var viewPreview: ViewExtender!
    @IBOutlet weak var imgPreview: UIImageView!
    
    var strBrand : String!
    var strImage : String!


    override func viewDidLoad() {
        super.viewDidLoad()

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblTitle.text = kCollection.uppercased()
        lblBrandName.text = strBrand.uppercased()
        
        let urlString = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: urlString!)
        imgPreview.setImageWith(url, usingActivityIndicatorStyle: .gray)
        imgPreview.contentMode = .scaleAspectFit

    }
    

    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)
    }


}
