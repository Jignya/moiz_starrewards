//
//  RegisterVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var lblEnter: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var txtMobileNum: UITextField!
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var btnNext: ButtonExtender!
    @IBOutlet weak var viewScroll: UIView!

    var countryId : NSNumber!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        txtMobileNum.delegate = self
        self.countryId = 1
        self.lblCode.text = "+965"
        
        lblEnter.text = kEnter
        lblMobile.text = kMobNum
        btnNext.setTitle(kNext, for: .normal)
        
        txtMobileNum.placeholder = kMobNum
        
        UserDefaults.standard.set(self.countryId, forKey: "country_id")
        UserDefaults.standard.synchronize()
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let leftAnimation = AnimationType.from(direction: .left, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])

        }
        else
        {
            let leftAnimation = AnimationType.from(direction: .right, offSet: 50.0)
            viewScroll.animateAll(withType: [leftAnimation])

        }

        
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        
    }
    
    
    // MARK: -Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNum
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "9"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        
        
        
        
        return true
    }
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    

    @IBAction func btnFlagClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let arrCountry = Manager.sharedInstance.arrcountry.mutableCopy()
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            self.countryId = (dictSelect["Country_Id"] as! NSNumber)
            UserDefaults.standard.set(self.countryId, forKey: "country_id")
            UserDefaults.standard.synchronize()
            self.lblCode.text = dictSelect["ISD_Code"] as? String
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dictSelect["Country_Image_Url"] as! String)
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            self.imgFlag.sd_setImage(with: url, placeholderImage: UIImage(named: ""))

            
        }, arrData: NSMutableArray(array: arrCountry as! Array), isLanguage: false, isNationality: true, isCategories: false, isProfession: false, isother: false,self)
    }
    
    @IBAction func btnNextClick(_ sender: Any)
    {
        self.view.endEditing(true)

        if txtMobileNum.text == ""
        {
            Manager.sharedInstance.showValidation(msg: kPleaseenteryourmobileEn, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)

            
//            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourNameEn, preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title:kOkEn, style: UIAlertActionStyle.default, handler: nil))
//            alert.show()
        }
        else if((txtMobileNum.text?.count)! < 8)
        {
            Manager.sharedInstance.showValidation(msg: kPleaseentervalidMobnum, Bgcolor: GlobalConstants.DarkTheme, textcolor: UIColor.white)
        }
        else
        {
            wscallforRegister()
        }
        
    }
    
    //MARK:- webservice call
    func wscallforRegister()
    {
        let strNumber = String(format: "%@%@", self.lblCode.text! , self.txtMobileNum.text!)
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegisterMobile)
        
        let dictPara:NSDictionary = ["Mobile":strNumber]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        UserDefaults.standard.set(self.txtMobileNum.text!, forKey: "mobile")
                        UserDefaults.standard.set(self.lblCode.text!.replacingOccurrences(of: "+", with: ""), forKey: "code")

                          UserDefaults.standard.synchronize()
                        Manager.sharedInstance.wscallforArea()
                        let login = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
                        
                        login.strCode = result["VerificationCode"] as? String
                        self.navigationController?.pushViewController(login, animated: false)

                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
        
    }
    
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
