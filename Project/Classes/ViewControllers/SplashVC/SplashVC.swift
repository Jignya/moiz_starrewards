//
//  SplashVC.swift
//  Pantone
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class SplashVC: UIViewController
{
    
    @IBOutlet weak var imgBg: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) != nil)
        {
        
                self.perform(#selector(pushToHomeScreen), with: self, afterDelay: 0.7)
        }
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func pushToHomeScreen()
    {
        Manager.sharedInstance.wscallforCountries()
        
        UserDefaults.standard.set("YES", forKey: GlobalConstants.kisFirstTime)
        UserDefaults.standard.synchronize()

        let storyboard1 : UIStoryboard!
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }
        
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            
            Manager.sharedInstance.wscallforArea()

            let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true

            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
            
        }
        else
        {

            let login = storyboard1.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            self.navigationController?.pushViewController(login, animated: false)

        }


        
    }
    
    
    
    
    

    
}
