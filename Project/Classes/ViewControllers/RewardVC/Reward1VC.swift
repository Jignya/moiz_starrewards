//
//  Reward1VC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class Reward1VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var lblYourRewards: UILabel!
    @IBOutlet weak var viewPoints: ViewExtender!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    
    @IBOutlet weak var lblPointearn: UILabel!
    @IBOutlet weak var lblPriceEarn: UILabel!
    @IBOutlet weak var lblCurrencyearn: UILabel!
    @IBOutlet weak var lblHowToEarn: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    
    @IBOutlet weak var concolListHeight: NSLayoutConstraint!

    
    @IBOutlet weak var lblToearn: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    var arrList = [[String:String]]()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblHowToEarn.text = kHowToEarn.uppercased()
        lblYourRewards.text = kYourRewards.uppercased()
        lblPointearn.text = kworthPointEarn.capitalized
        
        

//        viewPoints.setGradientBackground(view: viewPoints, colorTop: GlobalConstants.DarkTheme, colorBottom: GlobalConstants.lightTheme)
        
        arrList = [["name":kShopping,"image1":"inactive.png","image2":"active.png","isSelect":"1"] ,["name":kTellFrnds,"image1":"inactive1.png","image2":"active1.png","isSelect":"0"],["name":kCompleteProfile,"image1":"inactive2.png","image2":"active2.png","isSelect":"0"],["name":kOnattendingEvents,"image1":"inactive3.png","image2":"active3.png","isSelect":"0"]]
        
//        colList.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if Manager.sharedInstance.worthPoint != nil
        {
            lblPrice.text = Manager.sharedInstance.earnPoint
            lblCurrency.text = kCurrency
            lblPriceEarn.text = Manager.sharedInstance.worthPoint + " " + kCurrency
            lblCurrencyearn.text = ""

        }

    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "BrandCell"
        let cell:BrandCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! BrandCell
        
        let dict = arrList[indexPath.item]
        
        cell.lblTitle.text = dict["name"]?.uppercased()
        cell.lblNumber.isHidden = true
        
        if dict["isSelect"] == "1"
        {
            cell.lblTitle.textColor = UIColor.white
            cell.imgLogo.image = UIImage(named: dict["image1"]!)
        }
        else
        {
            cell.lblTitle.textColor = UIColor.lightGray
            cell.imgLogo.image = UIImage(named: dict["image2"]!)
        }
        
//        concolListHeight.constant = self.colList.contentSize.height
//        colList.updateConstraintsIfNeeded()
//        colList.layoutIfNeeded()
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if UserDefaults.standard.value(forKey: "isProfileComplete") as? NSNumber == 1 && indexPath.item == 2
        {
            return
        }
        
        
        DispatchQueue.main.async
        {
            for i in 0..<self.arrList.count
            {
                var dict = self.arrList[i]
                if i == indexPath.item
                {
                    dict["isSelect"] = "1"
                }
                else
                {
                    dict["isSelect"] = "0"
                }
                self.arrList[i] = dict
            }
            
            
            self.colList.reloadData()
        }
        
       
        

        if indexPath.item == 1
        {
            //            let items = ["Please download Star Rewards application"]
            //            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            //            present(ac, animated: true)
        }
            
        else if indexPath.item == 2
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailVC") as! UserDetailVC
            self.navigationController?.pushViewController(login, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width:CGFloat = (self.colList.frame.size.width / 2) - 5
        let cellSize = CGSize(width:width , height:width)
        return cellSize
    }
    
}
