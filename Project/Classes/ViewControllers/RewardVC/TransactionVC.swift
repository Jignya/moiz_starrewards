//
//  TransactionVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class TransactionVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var lblYourRewards: UILabel!
    @IBOutlet weak var viewPoints: ViewExtender!
    @IBOutlet weak var btnAddPoints: UIButton!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var colList: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    
    var arrList : NSMutableArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        
        wscallforTransaction()
        
        txtSearch.placeholder = kSearch

    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "TransactionCell1"
        let cell:TransactionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! TransactionCell
        
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        cell.lblInvoice.text = String(format: "Invoice No : %@", (dict["Invoice_No"] as? NSNumber)!)
        cell.lblDesc.text = String(format: "For : %@", (dict["Description"] as? String)!)
        cell.lblDate.text = String(format: "Date : %@", (dict["TXN_Date"] as? String)!)
        cell.lblAmount.text = String(format: "%@", (dict["Points"] as? NSNumber)!)
        
        
        

        
        if dict["POS_TXN_Type_Id"] as? NSNumber == 0 || dict["POS_TXN_Type_Id"] as? NSNumber == 0
        {
            
            let fullString = NSMutableAttributedString(string: "" )
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = #imageLiteral(resourceName: "up@2x.png")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " " ))
            fullString.append(NSAttributedString(string: cell.lblAmount.text!))
            
            cell.lblAmount.attributedText = fullString
            
            cell.lblAmount.textColor = UIColor(red: 66.0/255.0, green: 161.0/255.0, blue: 11.0/255.0, alpha: 1)
        }
        else
        {
            let fullString = NSMutableAttributedString(string: "" )
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = #imageLiteral(resourceName: "down_red@2x.png")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " " ))
            fullString.append(NSAttributedString(string: cell.lblAmount.text!))
            
            cell.lblAmount.attributedText = fullString
            cell.lblAmount.textColor = UIColor(red: 244.0/255.0, green: 33.0/255.0, blue: 69.0/255.0, alpha: 1)

        }
        
        
        if indexPath.item == 2
        {
            let fullString = NSMutableAttributedString(string: "" )
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = #imageLiteral(resourceName: "down_red@2x.png")
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " " ))
            fullString.append(NSAttributedString(string: cell.lblAmount.text!))
            
            cell.lblAmount.attributedText = fullString
            cell.lblAmount.textColor = UIColor(red: 244.0/255.0, green: 33.0/255.0, blue: 69.0/255.0, alpha: 1)
            
        }

        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width:CGFloat = self.view.frame.size.width - 40
        let cellSize = CGSize(width:width , height: 111)
        return cellSize
    }
    
    
    @IBAction func btnAddPointClick(_ sender: Any)
    {
        
    }
    
    func wscallforTransaction()
    {
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetTransactions,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Transactions"] as? Array)!)
                        self.colList.reloadData()

                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
}
