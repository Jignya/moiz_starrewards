//
//  TransactionCell.swift
//  Project
//
//  Created by HTNaresh on 22/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class TransactionCell: UICollectionViewCell
{
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblInvoice: UILabel!

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lbltime: UILabel!




    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        
    }
    
}
