//
//  NewsListVC.swift
//  Project
//
//  Created by HTNaresh on 22/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class NewsListVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    
    var arrList : NSMutableArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblTitle.text = kNews.uppercased()
        wscallforNews()
        
    }
    

    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "3")
    }

    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "TransactionCell"
        let cell:TransactionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! TransactionCell
        
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        cell.lblName.text = dict["Title"] as? String
        cell.lblDesc.text = dict["Short_Desc"] as? String
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
        detail.newsId = dict["News_Id"] as? NSNumber
        self.navigationController?.pushViewController(detail, animated: false)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let dict = arrList[indexPath.row] as! NSDictionary
        let nameString = dict["Short_Desc"] as! String
        let font : UIFont = UIFont.systemFont(ofSize: 15)
        
        let sizeOfText = nameString.height(withConstrainedWidth: (self.view.frame.size.width - 30), font: font)
        
        let cellSize = CGSize(width: (self.view.frame.size.width - 30), height:sizeOfText + 45)
        return cellSize
    }
    
    func wscallforNews()
    {
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNews,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["News"] as? Array)!)
                        self.colList.reloadData()
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
