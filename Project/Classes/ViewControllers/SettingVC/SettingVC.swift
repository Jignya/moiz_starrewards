//
//  RewardVC.swift
//  Project
//
//  Created by HTNaresh on 17/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.


import UIKit

class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var viewImgBg: ViewExtender!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var viewLanguage: UIView!
    @IBOutlet weak var conViewlangHeight: NSLayoutConstraint!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblTitle: UILabel!


    var arrList = [String]()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        conViewlangHeight.constant = 0
        
        btnArabic.setTitle(kArab, for: .normal)
        btnEnglish.setTitle(kEnglish, for: .normal)
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        
        arrList = [klang,kprofile,kTier,"Transfer Point","Scan Qr",kAbout,kLogout]
        
        
        lblMobile.text = "+ " + (UserDefaults.standard.value(forKey: "code") as? String)! + " " + (UserDefaults.standard.value(forKey: "mobile") as? String)!

        lblName.text = UserDefaults.standard.value(forKey: "name") as? String
        
        lblTitle.text = ksetting.uppercased()

        let infoDict = Bundle.main.infoDictionary
        lblVersion.text
            = String(format: "Version : %@", infoDict?["CFBundleShortVersionString"] as! CVarArg)


    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar1(self, tab: "4")
    }
    
    
    //MARK:- tableview Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "MalltableCell"
        let cell:MalltableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MalltableCell
        
        let str = arrList[indexPath.item]

        cell.lblTitle.text = str
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 1
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailVC") as! UserDetailVC
            self.navigationController?.pushViewController(login, animated: false)
        }
        else if indexPath.row == 0
        {
            self.conViewlangHeight.constant = 0
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.conViewlangHeight.constant = self.view.frame.size.height
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
        else if indexPath.row == 2
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
            login.strTitle = arrList[indexPath.item]
            self.navigationController?.pushViewController(login, animated: true)
        }
        else if indexPath.row == 3
        {
//            let login = self.storyboard?.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
//            login.strTitle = arrList[indexPath.item]
//            self.navigationController?.pushViewController(login, animated: true)
        }
        else if indexPath.row == 4
        {
           //QrCode
            
            let login = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeVC") as! QRCodeVC
            self.navigationController?.pushViewController(login, animated: true)

        }
        else if indexPath.row == 5
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
            login.strTitle = arrList[indexPath.item]
            self.navigationController?.pushViewController(login, animated: true)
        }
        else if indexPath.row == 6
        {
            AJAlertController.initialization().showAlert(aStrMessage: kLogoutalert, aCancelBtnTitle: kCancel, aOtherBtnTitle: kOkEn) { (index, title) in
                if index == 1
                {
                    print("ok")
                    self.callWsLogout()

                }
                else
                {
                    print("cancel")

                }
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40
    }
  
    @IBAction func btnArabicClick(_ sender: Any)
    {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conViewlangHeight.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: nil)
        
        if !UserDefaults.standard.bool(forKey: "arabic")
        {
            let storyboard1 : UIStoryboard!
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
            
            UserDefaults.standard.set(true, forKey: "arabic")
            UserDefaults.standard.synchronize()
            
            Manager.sharedInstance.wscallforArea()
            
            
            let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigation = UINavigationController()
            navigation.isNavigationBarHidden = true
            navigation.setViewControllers([home], animated: false)
            
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigation
        }
        
        
        
    }
    
    @IBAction func btnEnglishClick(_ sender: Any)
    {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conViewlangHeight.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: nil)
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            let storyboard1 : UIStoryboard!
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            
            UserDefaults.standard.set(false, forKey: "arabic")
            UserDefaults.standard.synchronize()
            
            Manager.sharedInstance.wscallforArea()
            
            
            let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigation = UINavigationController()
            navigation.isNavigationBarHidden = true
            navigation.setViewControllers([home], animated: false)
            
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigation
        }
        
       
    }
    
    
    //MARK: -Logout Api
    
    func callWsLogout()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var strToken = appDelegate.UUIDValue
        if strToken == ""
        {
            strToken = "1234"
        }
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wLogout)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        UserDefaults.standard.set(false, forKey: GlobalConstants.kisLogin)
                        UserDefaults.standard.removeObject(forKey: "userId")
                        UserDefaults.standard.synchronize()
                        
                        let storyboard1 : UIStoryboard!
                        if UserDefaults.standard.bool(forKey: "arabic")
                        {
                            UIView.appearance().semanticContentAttribute = .forceRightToLeft
                            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
                        }
                        else
                        {
                            UIView.appearance().semanticContentAttribute = .forceLeftToRight
                            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
                        }
                        
                        let home = storyboard1.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                        
                    }
                    else
                    {
                        AJAlertController.initialization().showAlert(aStrMessage: strStatus!, aCancelBtnTitle: "", aOtherBtnTitle: "") { (index, title) in
                            if index == 1
                            {
                            }
                        }
                    }
                    
            }
        }
    }
    
    
}
