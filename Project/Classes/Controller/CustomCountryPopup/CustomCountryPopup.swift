  //
  //  CustomCountryPopup.swift
  //  Maki
  //
  //  Created by Monali on 8/17/16.
  //  Copyright © 2016 olivermaki. All rights reserved.
  //
  
  import UIKit
  
  //   typedef void(^countrySelection)(NSMutableDictionary *dictSelect);
  
  typealias countrySelection = (NSDictionary) ->Void
  var dictSelect : countrySelection!
  var pSelectionAction:countrySelection!
  
  typealias countrySelection1 = (NSMutableArray) ->Void
  var dictSelect1 : countrySelection1!
  var pSelectionAction1:countrySelection1!
  
  class CustomCountryPopup: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var ConCenterY: NSLayoutConstraint!
    @IBOutlet var consTableHight: NSLayoutConstraint!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var TblCountry: UITableView!
    @IBOutlet var BlurViewContainer: UIView!
    @IBOutlet var TapView: UIView!
    @IBOutlet var MainView: UIView!
    
    @IBOutlet weak var ConviewSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var conbtnOkHeight: NSLayoutConstraint!
    
    var arrCountry : NSMutableArray = []
    var arrCountryOriginal : NSMutableArray = []
    var arrSelected : NSMutableArray = []
    
    var strPlaceHolder:String!
    var strCountry:String!
    var isCountrykey: Bool! = false
    var isindexpath: Bool! = false
    
    var isAllowMultiple : Bool! = false
    var keyName:String!
    var height: NSNumber = 0.0
    var window :UIWindow?
    
    var search:String = ""
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        window = UIApplication.shared.keyWindow!
        
        MainView.layer.cornerRadius = 8.0
        MainView.layer.borderColor = UIColor(red:0.76, green:0.76, blue:0.76, alpha:1.0).cgColor
        MainView.layer.borderWidth = 1.0
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.numberOfTapsRequired = 1
        self.TapView.addGestureRecognizer(tap)
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleTap()
    {
        CloseClick()
    }
    
    @IBAction func btnOkClick(_ sender: Any)
    {
        pSelectionAction1(arrSelected)
        self.CloseClick()
    }
    
    func CloseClick()
    {
        
        self.view.removeFromSuperview()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomCountryPopup.keyboardWillShow(_:)) , name: UIKeyboardWillShowNotification, object: self.view.window)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomCountryPopup.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: self.view.window)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: self.view.window)
        //        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: self.view.window)
        //        super.viewWillDisappear(animated)
    }
    
    //    func keyboardWillShow(sender: NSNotification) {
    //        let userInfo: [NSObject : AnyObject] = sender.userInfo!
    //
    //        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
    ////        let offset: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
    //        height = self.view.frame.size.height-keyboardSize.height-60
    //
    //        UIView.animateWithDuration(0.2, animations: {
    ////            self.MainView.transform = CGAffineTransformMakeScale(1.1, 1.1)
    //
    //            if(self.TblCountry.contentSize.height <= self.height as CGFloat) {
    //                if (self.TblCountry.contentSize.height<10) {
    //                    self.consTableHight.constant = 10+40;
    //                }
    //                self.consTableHight.constant = self.TblCountry.contentSize.height+40
    //            }
    //            else{
    //                self.consTableHight.constant = self.height as CGFloat
    //            }
    //
    //            self.ConCenterY.constant = -(self.height as CGFloat/2)
    //
    //            self.MainView.updateConstraintsIfNeeded()
    //            self.MainView.layoutIfNeeded()
    //
    //            self.TblCountry.updateConstraintsIfNeeded()
    //            self.TblCountry.layoutIfNeeded()
    //
    //        }) { _ in
    //
    //        }
    //
    //    }
    //
    //    func keyboardWillHide(sender: NSNotification) {
    //        height = 300
    //
    //        UIView.animateWithDuration(0.2, animations: {
    //            //            self.MainView.transform = CGAffineTransformMakeScale(1.1, 1.1)
    //
    //            if(self.TblCountry.contentSize.height <= self.height as CGFloat) {
    //                if (self.TblCountry.contentSize.height<10) {
    //                    self.consTableHight.constant = 10+40;
    //                }
    //                self.consTableHight.constant = self.TblCountry.contentSize.height+40
    //            }
    //            else{
    //                self.consTableHight.constant = self.height as CGFloat
    //            }
    //
    //            self.ConCenterY.constant = 0
    //
    //            self.MainView.updateConstraintsIfNeeded()
    //            self.MainView.layoutIfNeeded()
    //
    //            self.TblCountry.updateConstraintsIfNeeded()
    //            self.TblCountry.layoutIfNeeded()
    //
    //        }) { _ in
    //
    //        }
    //    }
    
    func initializeController()
    {
        TblCountry.isHidden = false
        TblCountry.reloadData()
        
        //        if self.TblCountry.contentSize.height < 10
        //        {
        //            self.consTableHight.constant = 50
        //        }
        //        else if(self.TblCountry.contentSize.height > 380)
        //        {
        //            self.consTableHight.constant = 380
        //        }
        //        else
        //        {
        //            self.consTableHight.constant = self.TblCountry.contentSize.height
        //        }
        //
        //        self.MainView .updateConstraints()
        //        self.MainView.updateConstraintsIfNeeded()
        //
        //        self.TblCountry.updateConstraints()
        //        self.TblCountry.updateConstraintsIfNeeded()
        
        MainView.layer.cornerRadius = 8.0
        MainView.layer.borderColor = UIColor(red:0.76, green:0.76, blue:0.76, alpha:1.0).cgColor
        MainView.layer.borderWidth = 1.0
        
        self.window?.addSubview(self.view)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrCountry.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:CountryPopupCell = tableView.dequeueReusableCell(withIdentifier: "CountryPopupCell")! as! CountryPopupCell
        
        let objectData = arrCountry.object(at: indexPath.row) as! NSDictionary
        
        cell.ConImgWidth.constant = 0
        
        if keyName == "Subscription_Name"
        {
            if let n = objectData.value(forKey: "Amount") as? NSNumber
            {
                let f = n.floatValue
                cell.lblTitle.text = String(format: "%@ (%.3f %@)", objectData.object(forKey: keyName) as! String,f, kCurrency)
                
            }
            else
            {
                cell.lblTitle.text = String(format: "%@ (%.3f %@)", objectData.object(forKey: keyName) as! String,objectData.object(forKey: "Amount") as! Float, kCurrency)
            }
            
        }
        else if keyName == "Country_Name"
        {
            cell.lblTitle.text = objectData.object(forKey: keyName) as? String

            cell.ConImgWidth.constant = 35
            
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,objectData["Country_Image_Url"] as! String)
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgFlag.sd_setImage(with: url, placeholderImage: UIImage(named: ""))


        }
        else
        {
            cell.lblTitle.text = objectData.object(forKey: keyName) as? String
        }
        
        
        cell.lblBackground.isHidden = true
        
        if(isAllowMultiple == true)
        {
            cell.ConImgWidth.constant = 14
            cell.ConLblLeading.constant = 7
            
            if arrSelected.contains(objectData)
            {
                cell.imgFlag.image = UIImage(named: "check1.png")
            }
            else
            {
                cell.imgFlag.image = UIImage(named: "uncheck1.png")
            }
            
            
            cell.lblTitle.updateConstraints()
            cell.lblTitle.layoutIfNeeded()
            
            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                cell.lblTitle.textAlignment = .right
            }
            else
            {
                cell.lblTitle.textAlignment = .left

                
            }
            
        }
        else
        {
            
            cell.ConLblLeading.constant = 7
            let heightTable : CGFloat = self.TblCountry.contentSize.height

            if heightTable > 260
            {
                consTableHight.constant = 260 + 40
            }
            else
            {
                if(ConviewSearchHeight.constant == 40)
                {
                    consTableHight.constant = heightTable + 40
                }
                else
                {
                    consTableHight.constant = heightTable
                    
                }
            }
            
            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                cell.lblTitle.textAlignment = .right
                txtSearch.textAlignment = .right

            }
            else
            {
                cell.lblTitle.textAlignment = .left
                txtSearch.textAlignment = .left

                
            }
        }
        
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:CountryPopupCell = tableView.cellForRow(at: indexPath) as! CountryPopupCell
        
        let objectData = arrCountry.object(at: indexPath.row) as! NSDictionary
        
        
        if  (isAllowMultiple)
        {
            if arrSelected.contains(objectData)
            {
                cell.imgFlag.image = UIImage(named: "uncheck1.png")
                arrSelected.remove(objectData)
            }
            else
            {
                arrSelected.add(objectData)
                cell.imgFlag.image = UIImage(named: "check1.png")
                
            }
            
        }
        else
        {
            // print("You selected cell #\(indexPath.row)!")
            tableView.deselectRow(at: indexPath, animated: true)
            pSelectionAction(objectData as! NSDictionary);
            CloseClick()
            
        }
        
        
        
    }
    
    // method table view height
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //
    //
    //        return 39;
    //
    //    }
    //
    // MARK:- textfield delegate
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField.text?.isEmpty)!
        {
            arrCountry = NSMutableArray(array: arrCountryOriginal)
            TblCountry.isHidden = false
            TblCountry.reloadData()
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if  string.isEmpty   //string
        {
            search = String(search.characters.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        
        let arrSubCatTemp = arrCountryOriginal.mutableCopy() as! [[String:Any]]
        
        let arrdata  = arrSubCatTemp.filter{
            let firstName = ($0[keyName]! as AnyObject).lowercased
            
            return firstName?.range(of: search.lowercased()) != nil
            
        }
        
        arrCountry = NSMutableArray(array: arrdata)
        
        if arrCountry.count > 0
        {
            TblCountry.isHidden = false
            TblCountry.reloadData()
        }
            
        else
        {
            if(search.isEmpty)
            {
                txtSearch.text = ""
                TblCountry.isHidden = false
                textField.resignFirstResponder()
                arrCountry = NSMutableArray(array: arrCountryOriginal)
                TblCountry.reloadData()
                
            }
            else
            {
                TblCountry.isHidden = true
                
            }
            
        }
        return true
    }
    
    
    
    func openView(_ completion: @escaping (_ dictSelect: NSDictionary) -> Void ,arrData:NSMutableArray,isLanguage:Bool ,isNationality:Bool , isCategories:Bool ,isProfession:Bool ,isother:Bool)
    {
        txtSearch.placeholder = kSearch
        self.conbtnOkHeight.constant = 0
        isAllowMultiple = false
        pSelectionAction = completion
        arrCountry = arrData
        arrCountryOriginal = arrData
        
        if(isNationality == true)
        {
            ConviewSearchHeight.constant = 40
            keyName = GlobalConstants.kisArabic ? "Country_Name" : "Country_Name"
        }
        else if(isProfession == true)
        {
            ConviewSearchHeight.constant = 40
            
            keyName = GlobalConstants.kisArabic ? "Area_Name" : "Area_Name"
        }
        else if(isCategories == true)
        {
            ConviewSearchHeight.constant = 40
            
            keyName = GlobalConstants.kisArabic ? "Nationality_Name" : "Nationality_Name"
        }
        else
        {
            ConviewSearchHeight.constant = 0
            
            keyName = "name"
        }
        self.initializeController()
        
    }
    
    func openView1(_ completion: @escaping (_ dictSelect: NSMutableArray) -> Void ,arrData:NSMutableArray,arrUserselected:NSMutableArray,isLanguage:Bool ,isNationality:Bool , isCategories:Bool ,isProfession:Bool ,isother:Bool)
    {
        
        isAllowMultiple = true
        self.conbtnOkHeight.constant = 50
        pSelectionAction1 = completion
        arrCountry = arrData
        arrCountryOriginal = arrData
        arrSelected = arrUserselected
        
        
        if(isLanguage == true)
        {
            keyName = GlobalConstants.kisArabic ? "Lang_Name_Ar" : "Lang_Name"
        }
        else if(isCategories == true)
        {
            keyName = GlobalConstants.kisArabic ? "Promoter_Category_Name_Ar" : "Promoter_Category_Name"
        }
        else
        {
            keyName = "name"
        }
        self.initializeController()
        
    }
    
    
  }
