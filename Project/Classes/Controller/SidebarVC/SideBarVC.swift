//
//  SideBarVC.swift
//  ViteBird
//
//  Created by HTNaresh on 4/7/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit
import Alamofire

class SideBarVC: UIViewController
{
//    @IBOutlet weak var tapView: UIView!
//    @IBOutlet weak var tblList: UITableView!
//    @IBOutlet weak var conLogOutHeight: NSLayoutConstraint!
//    @IBOutlet weak var lblLogin: UILabel!
//    @IBOutlet weak var viewLogin: UIView!
//    @IBOutlet weak var lblLogout: UILabel!
//    @IBOutlet weak var viewTable: UIView!
//
//    var strcomeFrom : String!
//    var arrlist = [[String:String]]()
//
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//
//        strcomeFrom = "home"
//
//        let SingleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureClick(_:)))
//        tapView.addGestureRecognizer(SingleFingerTap)
//
//    }
//
//
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//
//        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
//
//
//
//
//        if((UserDefaults.standard.object(forKey: GlobalConstants.kisLogin)) != nil && UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin))
//        {
//            viewLogin.isHidden = true
//            arrlist = [["name":kHome,"image":"home.png"],["name":kMyOrder,"image":"5.png"],["name":kMycart,"image":"cart.png"],["name":kwish,"image":"3.png"],["name":kNotification,"image":"notification.png"],["name":ksetting,"image":"1.png"],["name":kAppInside,"image":"about-app.png"]]
//
//        }
//        else
//        {
//            viewLogin.isHidden = false
//            arrlist = [["name":kHome,"image":"home.png"],["name":kMyOrder,"image":"5.png"],["name":kMycart,"image":"cart.png"],["name":kwish,"image":"3.png"],["name":kNotification,"image":"notification.png"],["name":ksetting,"image":"1.png"],["name":kAppInside,"image":"about-app.png"]]
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return arrlist.count + 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        if indexPath.row == 0
//        {
//            let cell:sidecell = tableView.dequeueReusableCell(withIdentifier: "cell") as! sidecell
//
//            if((UserDefaults.standard.object(forKey: GlobalConstants.kisLogin)) != nil && UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin))
//            {
//                cell.lblName.text = Manager.sharedInstance.dictprofile["FirstName"] as? String
//                cell.lblWelcome.text = kwelcome
//                cell.lblEmail.text = Manager.sharedInstance.dictprofile["Username"] as? String
//
//            }
//            else
//            {
//                cell.lblName.text = kGuest
//                cell.lblWelcome.text = kwelcome
//                cell.lblEmail.text = ""
//            }
//
//
//            return cell
//        }
//        else
//        {
//            let cellReuseIdentifier = "sidecell"
//
//            let cell:sidecell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell
//
//            cell.lbltitle.text = arrlist[indexPath.row - 1]["name"]
//
//            cell.img.image = UIImage(named: (arrlist[indexPath.row - 1]["image"])!)
//
//            return cell
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        if((UserDefaults.standard.object(forKey: GlobalConstants.kisLogin)) != nil && UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin))
//        {
//            if indexPath.row == 1
//            {
////                if strcomeFrom != "home"
////                {
////                    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
////                    self.navigationController?.pushViewController(home, animated: true)
////                }
//            }
//            else if indexPath.row == 2
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//
//            else if indexPath.row == 3
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 4
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 5
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 6
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 7
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "Privacy_TermsVC") as! Privacy_TermsVC
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//
//        }
//        else
//        {
//            if indexPath.row == 1
//            {
//                if strcomeFrom != "home"
//                {
//                    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                    self.navigationController?.pushViewController(home, animated: true)
//                }
//            }
//            else if indexPath.row == 2
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 3
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 4
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 5
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 6
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//            else if indexPath.row == 7
//            {
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "Privacy_TermsVC") as! Privacy_TermsVC
//                self.navigationController?.pushViewController(home, animated: true)
//            }
//
//        }
//
//
//
//        Hidesidemenu()
//
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        if indexPath.row == 0
//        {
//            return 100
//        }
//        else
//        {
//            return 44
//        }
//
//    }
//
//
//    @objc func tapGestureClick(_ tapGesture: UITapGestureRecognizer?)
//    {
//        Hidesidemenu()
//    }
//
//    @IBAction func btnLogoutClick(_ sender: Any)
//    {
//        Hidesidemenu()
//
//        let alertController = UIAlertController(title:kWarningEn, message:kLogoutalert, preferredStyle: .alert)
//
//        // Create the actions
//        let okAction = UIAlertAction(title:kOkEn, style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            NSLog("OK Pressed")
//
//            self.callWsLogout()
//
//        }
//
//        let CancelAction = UIAlertAction(title:kCancel, style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            NSLog("OK Pressed")
//        }
//        // Add the actions
//        alertController.addAction(okAction)
//        alertController.addAction(CancelAction)
//
//        // Present the controller
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    @IBAction func btnLoginClick(_ sender: Any)
//    {
//        Hidesidemenu()
//
//        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        home.strComeFrom = "side"
//        self.navigationController?.pushViewController(home, animated: false)
//    }
//
//    func Hidesidemenu()
//    {
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//
//            if UserDefaults.standard.bool(forKey: "arabic") == false
//            {
//                self.viewTable.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width/2 - 10, height: UIScreen.main.bounds.size.height)
//            }
//            else
//            {
//                self.viewTable.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width/2 - 10, height: UIScreen.main.bounds.size.height);
//            }
//
//            self.view.layoutIfNeeded()
//            self.view.backgroundColor = UIColor.clear
//        }, completion: { (finished) -> Void in
//            self.view.removeFromSuperview()
//            self.removeFromParentViewController()
//        })
//    }
//
//    //MARK: -Logout Api
//
//    func callWsLogout()
//    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        var strToken = appDelegate.UUIDValue
//        if strToken == ""
//        {
//            strToken = "1234"
//        }
//
//        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wLogout,strToken)
//
//        let dictPara = NSDictionary()
//        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
//
//            DispatchQueue.main.async
//                {
//
//                    print(result)
//                    let strStatus = result["Message"] as? String
//
//                    if strStatus == "Success"
//                    {
//                        self.viewWillAppear(true)
//                        UserDefaults.standard.set(false, forKey: GlobalConstants.kisLogin)
//                      UserDefaults.standard.removeObject(forKey: "userid")
//                        UserDefaults.standard.synchronize()
//
//                    }
//                    else
//                    {
//                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
//
//                        // Create the actions
//                        let okAction = UIAlertAction(title:kOkEn, style: UIAlertActionStyle.default) {
//                            UIAlertAction in
//                            NSLog("OK Pressed")
//                        }
//
//                        // Add the actions
//                        alertController.addAction(okAction)
//
//                        // Present the controller
//                        self.present(alertController, animated: true, completion: nil)
//                    }
//
//            }
//        }
//
//
//
//
//    }
//
//
    
}
