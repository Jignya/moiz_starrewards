//
//  sidecell.swift
//  ViteBird
//
//  Created by HTNaresh on 4/7/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class sidecell: UITableViewCell {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var img: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
