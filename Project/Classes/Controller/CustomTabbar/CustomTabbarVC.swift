//
//  CustomTabbarVC.swift
//  ATV
//
//  Created by HTNaresh on 3/19/18.
//  Copyright © 2018 HTNaresh. All rights reserved.
//

import UIKit

class CustomTabbarVC: UIViewController
{
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnReward: UIButton!
    @IBOutlet weak var btnMall: UIButton!
    @IBOutlet weak var btnNews: UIButton!
    @IBOutlet weak var btnMore: UIButton!

    
    @IBOutlet weak var btnHome1: UIButton!
    @IBOutlet weak var btnReward1: UIButton!
    @IBOutlet weak var btnMall1: UIButton!
    @IBOutlet weak var btnNews1: UIButton!
    @IBOutlet weak var btnMore1: UIButton!
    

    
    var storyboard1 : UIStoryboard!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnHome1.setTitle(khome.uppercased(), for: .normal)
        btnReward1.setTitle(kRewards.uppercased(), for: .normal)
        btnMall1.setTitle(kBrand.uppercased(), for: .normal)
        btnNews1.setTitle(kOffers.uppercased(), for: .normal)
        btnMore1.setTitle(kMere.uppercased(), for: .normal)

       
        btnHome.isSelected = true
        btnReward.isSelected = false
        btnMall.isSelected = false
        btnNews.isSelected = false
        btnMore.isSelected = false

        setTintColor()

        setLabel()
        
//        view1.setGradientBackground(view: view1, colorTop: GlobalConstants.DarkTheme, colorBottom: GlobalConstants.lightTheme)

        
    }
    
    
    func setTintColor()
    {
        let image = btnHome.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnHome.setImage(image, for: .selected)
        btnHome.tintColor = GlobalConstants.lightTheme
        
        let image1 = btnReward.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnReward.setImage(image1, for: .selected)
        btnReward.tintColor = GlobalConstants.lightTheme
        
        let image2 = btnMall.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnMall.setImage(image2, for: .selected)
        btnMall.tintColor = GlobalConstants.lightTheme
        
        
        let image3 = btnNews.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnNews.setImage(image3, for: .selected)
        btnNews.tintColor = GlobalConstants.lightTheme
        
        let image4 = btnMore.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnMore.setImage(image4, for: .selected)
        btnMore.tintColor = GlobalConstants.lightTheme

    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
    }
    
    func setLabel()
    {
//        lblProfile.text = kMyProfile
//        lblMagazine.text = kMagazine
//        lblSources.text = kMySources
//        lbllatest.text = kLatestNews
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnMoreClick(_ sender: Any)
    {
        btnHome.isSelected =  false
        btnReward.isSelected = false
        btnMall.isSelected = false
        btnNews.isSelected = false
        btnMore.isSelected = true
        
        btnHome1.isSelected = false
        btnReward1.isSelected = false
        btnMall1.isSelected = false
        btnNews1.isSelected = false
        btnMore1.isSelected = true


        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true

        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnNewsClick(_ sender: Any)
    {
        btnHome.isSelected = false
        btnReward.isSelected = false
        btnMall.isSelected = false
        btnNews.isSelected = true
        btnMore.isSelected = false
        
        btnHome1.isSelected = false
        btnReward1.isSelected = false
        btnMall1.isSelected = false
        btnNews1.isSelected = true
        btnMore1.isSelected = false


        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "OfferVC") as! OfferVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnMallClick(_ sender: Any)
    {
        btnHome.isSelected = false
        btnReward.isSelected = false
        btnMall.isSelected = true
        btnNews.isSelected = false
        btnMore.isSelected = false
        
        btnHome1.isSelected = false
        btnReward1.isSelected = false
        btnMall1.isSelected = true
        btnNews1.isSelected = false
        btnMore1.isSelected = false
        
        
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }
        
        let home = storyboard1.instantiateViewController(withIdentifier: "StoreVC") as! StoreVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
    
    
    
    @IBAction func btnHomeClick(_ sender: Any)
    {
        btnHome.isSelected = true
        btnReward.isSelected = false
        btnMall.isSelected = false
        btnNews.isSelected = false
        btnMore.isSelected = false
        
        btnHome1.isSelected = true
        btnReward1.isSelected = false
        btnMall1.isSelected = false
        btnNews1.isSelected = false
        btnMore1.isSelected = false


        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }
    
    @IBAction func btnRewardClick(_ sender: Any)
    {
        btnHome.isSelected = false
        btnReward.isSelected = true
        btnMall.isSelected = false
        btnNews.isSelected = false
        btnMore.isSelected = false
        
        btnHome1.isSelected = false
        btnReward1.isSelected = true
        btnMall1.isSelected = false
        btnNews1.isSelected = false
        btnMore1.isSelected = false


        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "RewardVC") as! RewardVC

        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true

        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }

}
