//
//  CartData.swift
//  Pantone
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import Foundation

class CartData
{
    var Cart: Array<String>?
    var Cartitems: Array<AnyObject>?
    
    init(Cart:Array<String>?,Cartitems: Array<AnyObject>?)
    {
        self.Cart = Cart
        self.Cartitems = Cartitems
    }
}
