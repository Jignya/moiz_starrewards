//
//  Cart.swift
//  Pantone
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import Foundation

class Cart
{
    var id: Int?
    var icon: String?
    var desc: String?
    var limit: Int?
    var next: Int?
    var aname: String?
    var specialaddon: Int?
    
    init(id: Int?,icon: String?,desc: String?,limit: Int?,next: Int?,aname: String?,specialaddon: Int?){
        
        self.id = id
        self.icon = icon
        self.desc = desc
        self.limit = limit
        self.next = next
        self.aname = aname
        self.specialaddon = specialaddon
        
    }

}
