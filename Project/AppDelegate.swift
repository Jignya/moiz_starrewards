//
//  AppDelegate.swift
//  Pantone
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.  //com.Innovasolution.starCo //com.innovasolution.Starrewards


import UIKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var storyboard : UIStoryboard!
    var navigationController = UINavigationController()
    
    var UUIDValue = String()
    var TokenValue = String()
    var NotiTokenString = String()
    
    fileprivate var reach:Reachability!
    var networkStatus = Reachability()!
    var isInternetConnected: Bool!
    var isFromSharing: Bool = false
    
    var NoInternet : NoInternetVC! = nil
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // Override point for customization after application launch.
        
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            Manager.sharedInstance.wscallforCustomerRewards()
            
        }
        
        Fabric.with([Crashlytics.self])
        
        isInternetConnected = true
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        IQKeyboardManager.shared.enable = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reach)
        do
        {
            self.reach = Reachability()
            try reach.startNotifier()
        }
        catch
        {
            print("could not start reachability notifier")
        }
        
        if(isInternetConnected == true)
        {
            removesubview()
        }
        
        UserDefaults.standard.set(true, forKey: "endtimer")

        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) == nil)
        {
            UserDefaults.standard.set(0, forKey: "isProfileComplete")
            UserDefaults.standard.set(true, forKey: "not")
            UserDefaults.standard.set(false, forKey: "arabic")
            
            UserDefaults.standard.synchronize()
            self.perform(#selector(AppDelegate.WSCallFordeviceRegistration), with: self, afterDelay: 0.4)
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        return true
    }
    
    
    
   
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var notif = String()
        for i in 0..<deviceToken.count {
            notif += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        NotiTokenString = notif
        print("NotiTokenString::\(NotiTokenString)")
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) == nil)
        {
            self.perform(#selector(WSCallFordeviceRegistration), with: self, afterDelay:0.0)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])
    {
        if let aps = userInfo["aps"] as? NSDictionary
        {
            
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    @objc func reachabilityChanged(note: Notification)
    {
        
        let reachability = note.object as! Reachability

        switch reachability.connection
        {
        case .wifi:
            print("Reachable via WiFi")
            isInternetConnected = true
            removesubview()

        case .cellular:
            print("Reachable via Cellular")
            isInternetConnected = true
            removesubview()


        case .none:
            print("Network not reachable")
            self.isInternetConnected = false

            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                self.storyboard = UIStoryboard(name: "Main", bundle: nil)
            }
            else
            {
                self.storyboard = UIStoryboard(name: "Main", bundle: nil)
            }
            self.NoInternet = self.storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC

            self.NoInternet.view.tag = 2304
            DispatchQueue.main.async(execute: { () -> Void in

                self.window?.addSubview(self.NoInternet.view)
                self.NoInternet.view.translatesAutoresizingMaskIntoConstraints = false

                self.window!.addConstraints(
                    [NSLayoutConstraint(item: self.NoInternet.view, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal,toItem: self.window, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.window, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.window, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.window, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1.0, constant: 0.0)])
            })

        }
    }
    
    func removesubview()
    {
        if( isInternetConnected == true)
        {
            let subviews: NSArray = self.window!.subviews as NSArray
            for id in subviews
            {
                let view:UIView   =  id as! UIView
                if(view.tag == 2304)
                {
                    DispatchQueue.main.async(execute: {
                        (id as AnyObject).removeFromSuperview()
                        print("Removed")
//
//                        let splash1 = UIApplication.topViewController() as! UIViewController
//                        if (splash1.isKind(of: SplashVC.self))
//                        {
//                            let splash2 = splash1 as? SplashVC
//                            splash2?.pushToHomeScreen()
//                        }
                    })
                }
            }
        }
    }
    
    @objc func WSCallFordeviceRegistration()
    {
        var base64Encoded:String = ""
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey)) == nil)
        {
            UserDefaults.standard.set(false, forKey: "arabic")
            
            let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            let allowedCharsCount = UInt32(allowedChars.count)
            var randomString = ""
            
            for _ in (0..<16) {
                let randomNum = Int(arc4random_uniform(allowedCharsCount))
                let newCharacter = allowedChars[allowedChars.characters.index(allowedChars.startIndex, offsetBy: randomNum)]
                randomString += String(newCharacter)
            }
            let str = randomString
            let utf8str = str.data(using: String.Encoding.utf8)
            base64Encoded = (utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))! as String
            UserDefaults.standard.set(base64Encoded, forKey: GlobalConstants.kStoreKey)
            //            UserDefaults.standard.set("YES", forKey: GlobalConstants.kisFirstTime)
            UserDefaults.standard.synchronize()
        }
        else
        {
            base64Encoded = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        }
        UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        
        let tokenstr = String(format: "%@",NotiTokenString)
        
        let dictPara:NSDictionary = ["DeviceType":"1","UniqueDeviceId":UUIDValue,"NotificationToken": (tokenstr != "") ? NotiTokenString : "1234","ClientKey":base64Encoded]
        let strUrl:String = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegisterDevice)
        //  print(strUrl)
        print(dictPara)
        WebServiceManager.sharedInstance.postWebServiceWithoutHeaderAlamofire(strUrl, parameter: dictPara as! Dictionary<String, String> as Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "")
        { (result) in
            
            //            print(result)
            DispatchQueue.main.async
                {
                    if (tokenstr != "")
                    {
                       let splash1 = UIApplication.topViewController() as! UIViewController
                        if (splash1.isKind(of: SplashVC.self))
                        {
                            let splash2 = splash1 as? SplashVC
                            splash2?.pushToHomeScreen()
                        }
                    }
            }
        }
    }
}

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}


